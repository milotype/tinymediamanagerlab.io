---
permalink: /privacy/
title: "Privacy Policy"
author_profile: true
toc: true
---
# Privacy Policy
We created this Privacy Policy (version 2021-05-31), to declare which information we collect, how we use data and which options the users of our website have, according to the guidelines of the General Data Protection Regulation (EU) 2016/679

Unfortunately, these subjects sound rather technical due to their nature, but we have put much effort into describing the most important things as simply and clearly as possible.

## Automatic Data Retention
Every time you visit a website nowadays, certain information is automatically created and saved, just as it happens on this website.

Whenever you visit our website such as you are doing right now, our webserver (computer on which this website is saved/stored) automatically saves data such as

- the address (URL) of the accessed website
- browser and browser version
- the used operating system
- the address (URL) of the previously visited site (referrer URL)
- the host name and the IP-address of the device the website is accessed from
- date and time
in files (webserver-logfiles).

Generally, webserver-logfiles stay saved for two weeks and then get deleted automatically. We do not pass this information to others, but we cannot exclude the possibility that this data will be looked at in case of illegal conduct.

## Cookies
Our website does not use first-party cookies, but your Merchant of Record ([Paddle.com](https://www.paddle.com)) uses HTTP-cookies for the purchase process (only on the [/purchase](/purchase/) page).
For your better understanding of the following Privacy Policy statement, we will explain to you below what cookies are and why they are in use.

### What exactly are cookies?
Every time you surf the internet, you use a browser. Common browsers are for example Chrome, Safari, Firefox, Internet Explorer and Microsoft Edge. Most websites store small text-files in your browser. These files are called cookies.

What should not be dismissed, is that cookies are very useful little helpers. Nearly all websites use cookies. More accurately speaking these are HTTP-cookies, since there are also different cookies for other uses. http-cookies are small files which our website stores on your computer. These cookie files are automatically put into the cookie-folder, which is like the “brain” of your browser. A cookie consists of a name and a value. Moreover, to define a cookie, one or multiple attributes must be specified.

Cookies save certain parts of your user data, such as e.g. language or personal page settings. When you re-open our website, your browser submits these “user specific” information back to our site. Thanks to cookies, our website knows who you are and offers you the settings you are familiar to. In some browsers every cookie has its own file, in others such as Firefox, all cookies are stored in one single file.

There are both first-party cookies and third-party coookies. First-party cookies are created directly by our site, while third-party cookies are created by partner-websites (e.g. Google Analytics). Every cookie is individual, since every cookie stores different data. The expiration time of a cookie also varies – it can be a few minutes, or up to a few years. Cookies are no software-programs and contain no computer viruses, trojans or any other malware. Cookies also cannot access your PC’s information.

This is an example of how cookie-files can look:

**name:** _ga  
**value:** GA1.2.1326744211.152121406501-9  
**purpose:** differentiation between website visitors  
**expiration date:** after 2 years

A browser should support these minimum sizes:

- at least 4096 bytes per cookie
- at least 50 cookies per domain
- at least 3000 cookies in total

### Which types of cookies are there?
What exact cookies we use, depends on the used services. We will explain this in the following sections of the Privacy Policy statement. Firstly, we will briefly focus on the different types of HTTP-cookies.

There are 4 different types of cookies:

_Essential Cookies_  
These cookies are necessary to ensure the basic function of a website. They are needed when a user for example puts a product into their shopping cart, then continues surfing on different websites and comes back later in order to proceed to the checkout. Even when the user closed their window priorly, these cookies ensure that the shopping cart does not get deleted.

_Purposive Cookies_  
These cookies collect info about the user behavior and record if the user potentially receives any error messages. Furthermore, these cookies record the website’s loading time as well as its behavior within different browsers.

_Target-orientated Cookies_  
These cookies care for an improved user-friendliness. Thus, information such as previously entered locations, fonts or data in forms stay saved.

_Advertising Cookies_  
These cookies are also known as targeting-Cookies. They serve the purpose of delivering individually adapted advertisements to the user. This can be very practical, but also rather annoying.

Upon your first visit to a website you are usually asked which of these cookie-types you want to accept. Furthermore, this decision will of course also be saved in a cookie.

### How can I delete cookies?
You yourself take the decision if and how you want to use cookies. Thus, no matter what service or website cookies are from, you always have the option to delete, deactivate or only partially allow them. Therefore, you can for example block cookies of third parties but allow any other cookies.

If you want change or delete cookie-settings and would like to determine which cookies have been saved to your browser, you can find this info in your browser-settings:

[Chrome: Clear, enable and manage cookies in Chrome](https://support.google.com/chrome/answer/95647?tid=121406501)

[Safari: Manage cookies and website data in Safari](https://support.apple.com/en-gb/guide/safari/sfri11471/mac?tid=121406501)

[Firefox: Clear cookies and site data in Firefox](https://support.mozilla.org/en-US/kb/clear-cookies-and-site-data-firefox?tid=121406501)

[Internet Explorer: Delete and manage cookies](https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete-manage-cookies?tid=121406501)

[Microsoft Edge: Delete cookies in Microsoft Edge](https://support.microsoft.com/en-gb/help/4027947/microsoft-edge-delete-cookies?tid=121406501)

If you generally do not want to allow any cookies at all, you can set up your browser in a way, to notify you whenever a potential cookie is about to be set. This gives you the opportunity to manually decide to either permit or deny the placement of every single cookie. The settings for this differ from browser to browser. Therefore, it might be best for you to search for the instructions in Google. If you are using Chrome, you could for example put the search phrase “delete cookies Chrome” or “deactivate cookies Chrome” into Google.

### How is my data protected?
There is a “cookie policy” that has been in place since 2009. It states that the storage of cookies requires the user’s consent. However, among the countries of the EU, these guidelines are often met with mixed reactions. In Austria the guidelines have been implemented in § 96 section 3 of the Telecommunications Act (TKG).

If you want to learn more about cookies and do not mind technical documentation, we recommend [https://tools.ietf.org/html/rfc6265](https://tools.ietf.org/html/rfc6265), the Request for Comments of the Internet Engineering Task Force (IETF) called “HTTP State Management Mechanism”.

## Rights in accordance with the General Data Protection Regulation
You are granted the following rights in accordance with the provisions of the [GDPR](https://gdpr-info.eu/) (General Data Protection Regulation) and the [Austrian Data Protection Act (DSG)](https://www.ris.bka.gv.at/Dokumente/Erv/ERV_1999_1_165/ERV_1999_1_165.html):

- right to rectification (article 16 GDPR)
- right to erasure (“right to be forgotten“) (article 17 GDPR)
- right to restrict processing (article 18 GDPR)
- righ to notification – notification obligation regarding rectification or erasure of personal data or restriction of processing (article 19 GDPR)
- right to data portability (article 20 GDPR)
- right to object (article 21 GDPR)
- right not to be subject to a decision based solely on automated processing – including profiling – (article 22 GDPR)

If you think that the processing of your data violates the data protection law, or that your data protection rights have been infringed in any other way, you can lodge a complaint with your respective regulatory authority. For Austria this is the data protection authority, whose website you can access at [https://www.data-protection-authority.gv.at/](https://www.data-protection-authority.gv.at/).

## Third party data processors

### Paddle.com
We use [Paddle.com](https://paddle.com) from Paddle.com Market Limited, Judd House, 18-29 Mora Street, London, EC1V 8BT, United Kingdom to process all payments.

When opening a payment transaction, we share the following personal information with Paddle.com:

* Email address

Paddle.com is a recipient of your personal data and acts as a processor for us as far as the processing of payment transactions is concerned.

Your data will be processed as long as there is a corresponding consent. Apart from that, they will be deleted after the termination of the contract between us and Paddle.com, unless legal requirements make further storage necessary.

Paddle.com has implemented compliance measures for international data transfers. These apply to all global activities where Paddle.com processes personal data of individuals in the EU. These measures are based on the EU Standard Contractual Clauses (SCCs). For more information, please visit: https://www.paddle.com/legal/privacy.

## TLS encryption with https
We use https to transfer information on the internet in a tap-proof manner (data protection through technology design Article 25 Section 1 GDPR). With the use of TLS (Transport Layer Security), which is an encryption protocol for safe data transfer on the internet, we can ensure the protection of confidential information. You can recognise the use of this safeguarding tool by the little lock-symbol, which is situated in your browser’s top left corner, as well as by the use of the letters https (instead of http) as a part of our web address.

## Google Fonts Privacy Policy
On our website we use Google Fonts, from the company Google Inc. (1600 Amphitheatre Parkway Mountain View, CA 94043, USA).

To use Google Fonts, you must log in and set up a password. Furthermore, no cookies will be saved in your browser. The data (CSS, Fonts) will be requested via the Google domains fonts.googleapis.com and fonts.gstatic.com. According to Google, all requests for CSS and fonts are fully separated from any other Google services. If you have a Google account, you do not need to worry that your Google account details are transmitted to Google while you use Google Fonts. Google records the use of CSS (Cascading Style Sheets) as well as the utilised fonts and stores these data securely. We will have a detailed look at how exactly the data storage works.

### What are Google Fonts?
Google Fonts (previously Google Web Fonts) is a list of over 800 fonts which [Google LLC](https://en.wikipedia.org/wiki/Google?tid=121406501) provides its users for free.

Many of these fonts have been published under the SIL Open Font License license, while others have been published under the Apache license. Both are free software licenses.

### Why do we use Google Fonts on our website?
With Google Fonts we can use different fonts on our website and do not have to upload them to our own server. Google Fonts is an important element which helps to keep the quality of our website high. All Google fonts are automatically optimised for the web, which saves data volume and is an advantage especially for the use of mobile terminal devices. When you use our website, the low data size provides fast loading times. Moreover, Google Fonts are secure Web Fonts. Various image synthesis systems (rendering) can lead to errors in different browsers, operating systems and mobile terminal devices. These errors could optically distort parts of texts or entire websites. Due to the fast Content Delivery Network (CDN) there are no cross-platform issues with Google Fonts. All common browsers (Google Chrome, Mozilla Firefox, Apple Safari, Opera) are supported by Google Fonts, and it reliably operates on most modern mobile operating systems, including Android 2.2+ and iOS 4.2+ (iPhone, iPad, iPod). We also use Google Fonts for presenting our entire online service as pleasantly and as uniformly as possible.

### Which data is saved by Google?
Whenever you visit our website, the fonts are reloaded by a Google server. Through this external cue, data gets transferred to Google’s servers. Therefore, this makes Google recognise that you (or your IP-address) is visiting our website. The Google Fonts API was developed to reduce the usage, storage and gathering of end user data to the minimum needed for the proper depiction of fonts. What is more, API stands for „Application Programming Interface“ and works as a software data intermediary.

Google Fonts stores CSS and font requests safely with Google, and therefore it is protected. Using its collected usage figures, Google can determine how popular the individual fonts are. Google publishes the results on internal analysis pages, such as Google Analytics. Moreover, Google also utilises data of ist own web crawler, in order to determine which websites are using Google fonts. This data is published in Google Fonts’ BigQuery database. Enterpreneurs and developers use Google’s webservice BigQuery to be able to inspect and move big volumes of data.

One more thing that should be considered, is that every request for Google Fonts automatically transmits information such as language preferences, IP address, browser version, as well as the browser’s screen resolution and name to Google’s servers. It cannot be clearly identified if this data is saved, as Google has not directly declared it.

### How long and where is the data stored?
Google saves requests for CSS assets for one day in a tag on their servers, which are primarily located outside of the EU. This makes it possible for us to use the fonts by means of a Google stylesheet. With the help of a stylesheet, e.g. designs or fonts of a website can get changed swiftly and easily.

Any font related data is stored with Google for one year. This is because Google’s aim is to fundamentally boost websites’ loading times. With millions of websites referring to the same fonts, they are buffered after the first visit and instantly reappear on any other websites that are visited thereafter. Sometimes Google updates font files to either reduce the data sizes, increase the language coverage or to improve the design.

How can I delete my data or prevent it being stored?
The data Google stores for either a day or a year cannot be deleted easily. Upon opening the page this data is automatically transmitted to Google. In order to clear the data ahead of time, you have to contact Google’s support at [https://support.google.com/?hl=en-GB&tid=121406501](https://support.google.com/?hl=en-GB&tid=121406501). The only way for you to prevent the retention of your data is by not visiting our website.

Unlike other web fonts, Google offers us unrestricted access to all its fonts. Thus, we have a vast sea of font types at our disposal, which helps us to get the most out of our website. You can find out more answers and information on Google Fonts at [https://developers.google.com/fonts/faq?tid=121406501](https://developers.google.com/fonts/faq?tid=121406501). While Google does address relevant elements on data protection at this link, it does not contain any detailed information on data retention.
It proofs rather difficult to receive any precise information on stored data by Google.

On [https://policies.google.com/privacy?hl=en-GB](https://policies.google.com/privacy?hl=en-GB) you can read more about what data is generally collected by Google and what this data is used for.

## Embedded Social Media elements Privacy Policy
We have embedded elements from social media services on our website, to display pictures, videos and texts. By visiting pages that present such elements, data is transferred from your browser to the respective social media service, where it is stored. We do not have access to this data.
The following links lead to the respective social media services’ sites, where you can find a declaration on how they handle your data:

- Instagram Data Policy: [https://help.instagram.com/519522125107875](https://help.instagram.com/519522125107875)
- For YouTube, the Google Privacy Policy applies: [https://policies.google.com/privacy?hl=en-GB](https://policies.google.com/privacy?hl=en-GB)
- Facebook Data Policy: [https://www.facebook.com/about/privacy](https://www.facebook.com/about/privacy)
- Twitter Privacy Policy: [https://twitter.com/en/privacy](https://twitter.com/en/privacy)

## jQuery CDN Privacy Policy
We use jQuery CDN services by the jQuery Foundation to deliver our website and our subpages to you quickly and easily on different devices. jQuery is distributed via the Content Delivery Network (CDN) of the American software company StackPath (LCC 2012 McKinney Ave. Suite 1100, Dallas, TX 75201, USA). This service stores, manages and processes your personal data.

A content delivery network (CDN) is a network of regionally distributed servers that are connected to each other via the Internet. Through this network content and especially very large files, can be delivered quickly – even in peak demand periods.

jQuery uses JavaScript libraries to be able to deliver our website content quickly. For this, a CDN server loads the necessary files. As soon as a connection to the CDN server is established, your IP address is recorded and stored. This only happens if the data has not already been saved in your browser during a previous website visit.

StackPath’s privacy policy explicitly mentions that StackPath uses aggregated and anonymised data of various services (such as jQuery) for both, security enhancement and its own services. However, it is impossible for you to be personally identified with the use of this data.

If you want to avoid this data transfer, you always have the option to use JavaScript blockers such as ghostery.com or noscript.net. You can also simply deactivate the execution of JavaScript codes in your browser. If you decide to deactivate JavaScript codes, the usual functions will also change. For example, websites may no longer load as quickly.

StackPath is an active participant in the EU-U.S. Privacy Shield Framework, which regulates the correct and secure transfer of personal data. You can find more information at [https://www.privacyshield.gov/participant?id=a2zt0000000CbahAAC&status=Active](https://www.privacyshield.gov/participant?id=a2zt0000000CbahAAC&status=Active).
Also, you can find more information about StackPath’s data protection at [https://www.stackpath.com/legal/privacy-statement/](https://www.stackpath.com/legal/privacy-statement/) and jQuery’s data protection at [https://openjsf.org/wp-content/uploads/sites/84/2019/11/OpenJS-Foundation-Privacy-Policy-2019-11-15.pdf](https://openjsf.org/wp-content/uploads/sites/84/2019/11/OpenJS-Foundation-Privacy-Policy-2019-11-15.pdf).

## Cloudflare Privacy Policy
We use Cloudflare by the company Cloudflare, Inc. (101 Townsend St., San Francisco, CA 94107, USA) on this website to enhance its speed and security. For this, Cloudflare uses cookies and processes user data. Cloudflare, Inc. is an American company that offers a content delivery network and various security services. These services take place between the user and our hosting provider. In the following, we will try to explain in detail what all this means.

### What is Cloudflare?
A content delivery network (CDN), as provided by Cloudflare, is nothing more than a network of servers that are connected to each other. Cloudflare has deployed servers around the world, which ensure websites can appear on your screen faster. Simply put, Cloudflare makes copies of our website and places them on its own servers. Thus, when you visit our website, a load distribution system ensures that the main part of our website is delivered by a server that can display our website to you as quickly as possible. The CDN significantly shortens the route of the transmitted data to your browser. Thus, Cloudflare does not only deliver our website’s content from our hosting server, but from servers from all over the world. Cloudflare is particularly helpful for users from abroad, since pages can be delivered from a nearby server. In addition to the fast delivery of websites, Cloudflare also offers various security services, such as DDoS protection, or the web application firewall.

### Why do we use Cloudflare on our website?
Of course, we want our website to offer you the best possible service. Cloudflare helps us make our website faster and more secure. Cloudflare offers us web optimisations as well as security services such as DDoS protection and a web firewall. Moreover, this includes a Reverse-Proxy and the content distribution network (CDN). Cloudflare blocks threats and limits abusive bots as well as crawlers that waste our bandwidth and server resources. By storing our website in local data centres and blocking spam software, Cloudflare enables us to reduce our bandwidth usage by about 60%. Furthermore, the provision of content through a data centre near you and certain web optimizations carried out there, cut the average loading time of a website in about half. According to Cloudflare, the setting “I’m Under Attack Mode” can be used to mitigate further attacks by displaying a JavaScript calculation task that must be solved before a user can access a website. Overall, this makes our website significantly more powerful and less susceptible to spam or other attacks.

### What data is stored by Cloudflare?
Cloudflare generally only transmits data that is controlled by website operators. Therefore, Cloudflare does not determine the content, but the website operator themselves does. Additionally, Cloudflare may collect certain information about the use of our website and may process data we send or data which Cloudflare has received certain instructions for. Mostly, Cloudflare receives data such as IP addresses, contacts and protocol information, security fingerprints and websites’ performance data. Log data for example helps Cloudflare identify new threats. That way, Cloudflare can ensure a high level of security for our website. As part of their services, Cloudflare process this data in compliance with the applicable laws. Of course, this also includes the compliance with the General Data Protection Regulation (GDPR).

Furthermore, Cloudflare uses a cookie for security reasons. The cookie (__cfduid) is used to identify individual users behind a shared IP address, and to apply security settings for each individual user. The cookie is very useful, if you e.g. use our website from a restaurant where several infected computers are located. However, if your computer is trustworthy, we can recognise that with the cookie. Hence, you will be able to freely and carelessly surf our website, despite the infected PCs in your area. Another point that is important to know, is that this cookie does not store any personal data. The cookie is essential for Cloudflare’s security functions and cannot be deactivated.

### Cookies from Cloudflare
**Name:** __cfduid  
**Value:** d798bf7df9c1ad5b7583eda5cc5e78121406501-3  
**Purpose:** Security settings for each individual visitor  
**Expiry date:** after one year  

Cloudflare also works with third parties. They may however only process personal data after the instruction of Cloudflare and in accordance with the data protection guidelines and other confidentiality and security measures. Without explicit consent from us, Cloudflare will not pass on any personal data.

### How long and where is the data stored?
Cloudflare stores your information primarily in the United States and the European Economic Area. Cloudflare can transfer and access the information described above, from all over the world. In general, Cloudflare stores domains’ user-level data with the Free, Pro and Business versions for less than 24 hours. For enterprise domains that have activated Cloudflare Logs (previously called Enterprise LogShare or ELS), data can be stored for up to 7 days. However, if IP addresses trigger security warnings in Cloudflare, there may be exceptions to the storage period mentioned above.

### How can I delete my data or prevent data retention?
Cloudflare only keeps data logs for as long as necessary and in most cases deletes the data within 24 hours. Cloudflare also does not store any personal data, such as your IP address. However, there is information that Cloudflare store indefinitely as part of their permanent logs. This is done to improve the overall performance of Cloudflare Resolver and to identify potential security risks. You can find out exactly which permanent logs are saved at [https://developers.cloudflare.com/1.1.1.1/commitment-to-privacy/privacy-policy/privacy-policy/](https://developers.cloudflare.com/1.1.1.1/commitment-to-privacy/privacy-policy/privacy-policy/). All data Cloudflare collects (temporarily or permanently) is cleared of all personal data. Cloudflare also anonymise all permanent logs.

In their privacy policy, Cloudflare state that they are not responsible for the content you receive. For example, if you ask Cloudflare whether you can update or delete content, Cloudflare will always refer to us as the website operator. You can also completely prevent the collection and processing of your data by Cloudflare, when you deactivate the execution of script-code in your browser, or if you integrate a script blocker to your browser.

Cloudflare is an active participant in the EU-U.S. Privacy Shield Framework, which regulates the correct and secure transfer of personal data. You can find more information on this at [https://www.privacyshield.gov/participant?id=a2zt0000000GnZKAA0](https://www.privacyshield.gov/participant?id=a2zt0000000GnZKAA0).

You can learn more on Cloudflare’s data protection at [https://www.cloudflare.com/en-gb/privacypolicy/](https://www.cloudflare.com/en-gb/privacypolicy/).

Source: Created with the Datenschutz Generator by [AdSimple® Blog Marketing](https://www.adsimple.at/blog-marketing/) in cooperation with [bauguide.at](https://www.bauguide.at/)
