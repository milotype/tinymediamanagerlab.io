---
layout: single
permalink: "/docs/versions"
title: "tinyMediaManager versions"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

## v4

tinyMediaManager v4 is available in two flavors: **FREE** and **PRO**. The differences are

| | &nbsp;&nbsp;&nbsp;&nbsp;FREE&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;PRO&nbsp;&nbsp;&nbsp;&nbsp; |
|---|:---:|:---:|
| **Scan** your **data sources**<br />&nbsp;&nbsp;&nbsp;for new/changed content | :heavy_check_mark: | :heavy_check_mark: |
| **Import** and **export** NFO files<br />&nbsp;&nbsp;&nbsp;common NFO formats for import and export are supported | :heavy_check_mark: | :heavy_check_mark: |
| **Edit** movies, TV show and episode metadata<br />&nbsp;&nbsp;&nbsp;to adopt everything to your needs | :heavy_check_mark: | :heavy_check_mark: |
| **Rename** movies, TV shows and episodes<br />&nbsp;&nbsp;&nbsp;change the folder and file names in your desired style | :heavy_check_mark: | :heavy_check_mark: |
| **Automatic updates**<br />&nbsp;&nbsp;&nbsp;receive automatic updates | :heavy_check_mark: | :heavy_check_mark: |
| **Export** movies, TV shows and episodes<br />&nbsp;&nbsp;&nbsp;export your data into HTML, CSV, XLS, ... | :heavy_check_mark: | :heavy_check_mark: |
| **Command line interface (CLI)**<br />&nbsp;&nbsp;&nbsp;use the CLI interface to integrate tinyMediaManager into batch jobs | :heavy_check_mark: | :heavy_check_mark: |
| **Enhanced aspect ratio detection**<br />&nbsp;&nbsp;&nbsp;analyze your media with FFmpeg to get the _real_ aspect ratio | :heavy_check_mark: | :heavy_check_mark: |
| **TheMovieDB (TMDB)** scraper<br />&nbsp;&nbsp;&nbsp;for localized movie/TV show metadata and artwork | :heavy_check_mark: | :heavy_check_mark: |
| **Internet Movie Database (IMDB)** scraper<br />&nbsp;&nbsp;&nbsp;for english movie/TV show metadata |   | :heavy_check_mark: |
| **TheTVDB.com (TVDB)** scraper<br />&nbsp;&nbsp;&nbsp;for localized TV show metadata and artwork |   | :heavy_check_mark: |
| **The Open Movie Database (OMDb API)** scraper<br />&nbsp;&nbsp;&nbsp;for english movie/TV show metadata |   | :heavy_check_mark: |
| **tinyMediaManager universal** scraper<br />&nbsp;&nbsp;&nbsp;combine the results of other scrapers into one scraper |   | :heavy_check_mark: |
| **Kodi** XML scraper<br />&nbsp;&nbsp;&nbsp;use Kodi XML scrapers directly in tinyMediaManager |   | :heavy_check_mark: |
| **Trakt.tv** scraper<br />&nbsp;&nbsp;&nbsp;for english movie/TV show metadata |   | :heavy_check_mark: |
| **MPDB.TV** scraper<br />&nbsp;&nbsp;&nbsp;private scraper for french movie metadata and artwork |   | :heavy_check_mark: |
| **OFDb.de** scraper<br />&nbsp;&nbsp;&nbsp;for german movie metadata |   | :heavy_check_mark: |
| **TVmaze.com** scraper<br />&nbsp;&nbsp;&nbsp;for englishTV show metadata |   | :heavy_check_mark: |
| **Fanart.tv** artwork scraper<br />&nbsp;&nbsp;&nbsp;download artwork from Fanart.tv |   | :heavy_check_mark: |
| **Trailer download**<br />&nbsp;&nbsp;&nbsp;download movie/TV show trailers |   | :heavy_check_mark: |
| **Subtitle download**<br />&nbsp;&nbsp;&nbsp;download subtitles for your movies/episodes |   | :heavy_check_mark: |
| **Trakt.tv** integration<br />&nbsp;&nbsp;&nbsp;synchronize your library with Trakt.tv |   | :heavy_check_mark: |
| **FFmpeg** integration<br />&nbsp;&nbsp;&nbsp;use FFmpeg for thumb generation |   | :heavy_check_mark: |

## v3

tinyMediaManager v3 (legacy) is still supported, but only urgent fixes and **no** new features will be added. The main differences between v3 and v4 are:

+ bundled JRE and rewritten launcher (no installed Java needed any more)
+ real HiDPI support (slightly changed look and feel to achieve correct UI scaling)
+ universal TV show scraper
+ better integration into Plex, Emby and Jellyfin
+ completely reworked libmediainfo usage:
  - full support of reading/writing mediainfo XML files
  - better support of disc structures (.iso and VIDEO_TS/BDMV/HVDVD_TS folders)
  - several fixes for reading data out of libmediainfo
+ enhanced filter management
+ new command line interface
+ mediainfo integration for Trakt.tv
+ download of TV show themes
+ support for Windows taskbar / macOS dock access
+ added a true regular expression search for movies/movie set/TV show titles
+ display missing movie in movie sets
+ display missing episodes in the season overview panel
+ provide artwork from TMDB for Trakt.tv search
+ added showlink support for movies
+ FFmpeg thumb creation
+ v4 got many fixes (performance, stability, memory) which are not in v3. To get a feeling what changed, have a look at [GitLab](https://gitlab.com/tinyMediaManager/tinyMediaManager/-/compare/v3...devel?from_project_id=9945251)