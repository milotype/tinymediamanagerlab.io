---
layout: single
permalink: "/docs/tvshows/nfo-formats"
title: "NFO Formats"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

This is a list of our supported NFO formats and some hints how to use that in the corresponding software.

## Kodi

[Kodi](https://kodi.tv/) is a free and open source media center which is able to play almost every media format out there. Kodi natively supports NFO files - all you have to do is to prepare them with tinyMediaManager and set your data source as [source in Kodi](https://kodi.wiki/view/Adding_video_sources). If you don't want Kodi also to contact external sites for meta data, you should set the "Information provider" to **Local information only**. In this case Kodi just reads your local NFO/artwork files.


## XBMC

XBMC is the legacy format of Kodi NFO files for Kodi/XBMC versions below v16 (and probably other 3rd party software which still relies on that old format).


## Jellyfin

[Jellyfin](https://jellyfin.org/) is a free and open source centralized media center which is able to stream a wide range of media formats to your devices. Jellyfin has a wide range of [clients](https://jellyfin.org/clients/) where you can stream your media to. Jellyfin natively supports NFO files - you just need to prepare them with tinyMediaManager and to set up your data source in the Jellyfin library. If you don't want Jellyfin also to contact external sites for meta data, you should deactivate "Fetch additional data from the internet".

## Plex

[Plex](https://www.plex.tv/) is a centralized media center is able to stream a wide range of media formats to your devices. Plex has a wide range of [clients](https://www.plex.tv/media-server-downloads/#plex-app) where you can stream your media to. Plex has no official support for NFO files, but with a third party metadata agent, you are able to read NFO files created by tinyMediaManager - [XBMCnfoMoviesImporter.bundle](https://github.com/gboudreau/XBMCnfoMoviesImporter.bundle) and [XBMCnfoTVImporter.bundle](https://github.com/gboudreau/XBMCnfoTVImporter.bundle).

## Emby

[Emby](https://emby.media/) is a centralized media center is able to stream a wide range of media formats to your devices. Emby as a wide range of [clients](https://emby.media/download.html) where you can stream your media to. Emby natively supports NFO files - you just need to prepare them with tinyMediaManager and to set up sour data source in Emby.

## MediaPortal

[MediaPortal](https://www.team-mediaportal.com/) is a free and open source media center which is able to play a wide range of different media formats. MediaPortal natively supports NFO files.