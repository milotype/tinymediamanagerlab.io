---
layout: single
permalink: "/docs/tvshows/tvshowpanel"
title: "TV Show Panel"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"

gallery1:
  - url: images/docs/tvshows/tvshows01.png
    image_path: images/docs/tvshows/tvshows01.png
gallery2:  
  - url: images/docs/tvshows/tvshows02.png
    image_path: images/docs/tvshows/tvshows02.png
gallery3:    
  - url: images/docs/tvshows/tvshows03.png
    image_path: images/docs/tvshows/tvshows03.png    
gallery4:    
  - url: images/docs/tvshows/tvshows04.png
    image_path: images/docs/tvshows/tvshows04.png
gallery5:    
  - url: images/docs/tvshows/tvshows05.png
    image_path: images/docs/tvshows/tvshows05.png
gallery6:    
  - url: images/docs/tvshows/tvshows06.png
    image_path: images/docs/tvshows/tvshows06.png
gallery7:    
  - url: images/docs/tvshows/tvshows07.png
    image_path: images/docs/tvshows/tvshows07.png
gallery8:    
  - url: images/docs/tvshows/tvshows08.png
    image_path: images/docs/tvshows/tvshows08.png    
gallery9:    
  - url: images/docs/tvshows/tvshows09.png
    image_path: images/docs/tvshows/tvshows09.png    
gallery10:    
  - url: images/docs/tvshows/tvshows10.png
    image_path: images/docs/tvshows/tvshows10.png    
---

The TV show panel is the main panel in the TV show management section. It consists of two main parts:

* A list of TV shows/seasons/episodes (or often called TV show tree - left hand side)
* TV show information (right hand side - if a TV show is selected).
  * Details - common TV show details like title, premiered date, certifications, ...
  * Cast
  * Media files - everything about the files of your TV show (not episodes! mainly artwork)
  * Artwork - a panel full with the artwork of your TV show
  * Trailer - a list of all scraped trailers (URLs and trailer files)
* Season information (right hand side - if a season is selected)
  * Details - Season artwork and all episodes
  * Media files - all associated media files of the episodes of the season
* Episode information (right hand side - if an episode is selected)  
  * Details - common episode details like title, season/episode number, aired date, certifications, ...
  * Cast and crew
  * Media files - everything about the files of your episode

{% include gallery id="gallery1" %}

## TV Show List ##

The TV show tree displays the whole information about your TV shows, seasons and episodes in a tree style. In addition of the tree hierarchy there are also some columns with extra data about the TV show, season or episode:

* Title (TV show, season, episode)
* Season count (TV show)
* Episode count (TV show, season)
* Rating (TV show, episode)
* Aired date (TV show, season, episode)
* Video format (Episode)
* Size of the video file(s) (Episode)
* NFO - is there a NFO for this TV show?
* Artwork - is there artwork? (TV show, season, episode)
* Subtitles - are there subtitles? (TV show, season, episode)
* Watched - has this episode/season/TV show been marked as watched? (TV show, season, episode)

Some of the indicator flags are aggregated up to their next hierarchy. For example the artwork flag is available for every level. To indicate that artwork in any sub-level is missing, we've introduced some sort of tri-state in the icons. If we take the artwork again for an example, there are three different icons available:

* ![Everything ok](/images/docs/checkmark.png) Everything is ok (on the same level and _all_ sub-levels)
* ![Current level ok](/images/docs/exclamation_mark.png) Everything is ok on the same lavel, but there are missing items in one or more sub-levels
* ![Not ok](/images/docs/cross.png) There are errors on the same level and probably also on sub-levels

## TV Show Information ##

### TV Show Details ###

{% include gallery id="gallery1" %}

The TV details panel shows the main artwork (poster and fanart) and holds all relevant data for the TV show itself from various sources:

* Title and original title
* Premiered year and date
* Certification
* Ids in several TV show databases (with direct links to the most common ones)
* Status (Continuing, ended)
* Runtime
* Studio
* Genres
* All set tags
* The path on the disk where the TV show is
* Any special note for this TV show
* Rating (the _main_ or personal rating)
* Certification logo
* Plot  

### Cast ###

{% include gallery id="gallery2" %}

The cast panel shows an overview over the main actors of the whole TV show. Actor entries may also contain an image of the person on the right hand side.

### Media Files ###

{% include gallery id="gallery3" %}

The media files panel shows a table containing all _media files_ which has been found for this TV show. This are mainly artwork files and the NFO. For every media file there will be some data in this table like:

* Filename
* File size (in MB)
* Type of the media file
* Any detectable codec
* Video/image resolution
* Runtime
* Subtitle language

### Artwork ###

{% include gallery id="gallery4" %}

The artwork panel is a collection of all existing artwork for a TV show.

### Trailer ###

{% include gallery id="gallery5" %}

The trailer panel shows a list of all trailers from the scraper (or NFO). You are able to watch the trailers from within this table or directly download it. At the moment there is only trailer per TV show possible - if you decide to download a second one, the first one will be overwritten.

## Season Information ##

### Season Details ###

{% include gallery id="gallery6" %}

In the season details you will find an overview of the season itself: The season artwork (poster, fanart and banner) and a list of all available episodes of this season.

### Media Files ###

{% include gallery id="gallery7" %}

The media files panel shows a table containing all _media files_ which has been found for episodes of this season. For every media file there will be some data in this table like:

* Filename
* File size (in MB)
* Type of the media file
* Any detectable codec
* Video/image resolution
* Runtime
* Subtitle language

## Episode Information ##

### Episode Details ###

{% include gallery id="gallery8" %}

The episode details panel shows the main artwork (seasons poster and episode thumb) and holds all relevant data for the episode itself from various sources:

* TV show title
* Episode title and original title
* Season and episode number
* Aired date
* All set tags
* Date added (to tinyMediaManager)
* The path on the disk where the episode is
* Any special note for this episode
* Rating (the _main_ or personal rating)
* Logo images for the most interesting facts of your episode file (video and audio facts)
* Plot  

### Episode Cast ###

{% include gallery id="gallery9" %}

The cast panel shows an overview over the:

* Directors
* Writers
* Actors

of the episode. Cast entries may also contain an image of the person on the right hand side.

### Media Files ###

{% include gallery id="gallery10" %}

The media files panel shows all relevant data of your files for this episode. The upper area of this panel shows where your episode is located on the disk (path), when it has been added (date added) and if this episode has been watched yet (watched).

The next section shows the information for the _video_ stream of your episode:

* Source (origin of the episode file)
* Runtime (which is reported from the video file itself)
* Video codec
* Frame rate
* Video resolution
* Video bitrate
* Video bit depth

After the video section there is a list of all found _audio_ streams (inside the video file and external). This table shows various data about each audio stream such as:

* Source (inside the video file or external)
* Codec
* Bitrate
* Language

Beneath the table with the audio streams there is a table containing information of all found _subtitles_. This able only contains the location of the subtitle (internal/external) and the detected language.

In the bottom of this panel there is a table containing all _media files_ which has been found for this episode. This will be the episode file itself, any found audio and subtitle files, the NFO file and the artwork. For every media file there will be some data in this table like:

* Filename
* File size (in MB)
* Type of the media file
* Any detectable codec
* Video/image resolution
* Runtime
* Subtitle language
