---
layout: single
permalink: "/docs/quickstart"
title: "Quickstart"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---
In this document you find a quick guidance for starting with tinyMediaManager. The instructions are for working with movies - but managing TV shows require almost the same functions/settings.

## Installing tinyMediaManager

You can download the most recent version of tinyMediaManager from our web server at [https://release.tinymediamanager.org][1]. You may find more details of the different versions at the [Installation Page][2]. 

## Set up data sources

After starting tinyMediaManager for the first time, you need to do some basic setup. Head over to the settings dialog via the menu "tinyMediaManager - Settings" on top of the tinyMediaManager window.

The [[Settings dialog|Settings]] is divided into three parts:
* [[General section|General Settings]] The section general settings contains all "module independent" settings for tinyMediaManager like UI language or font sizes
* [[Movie section|Movie Settings]] This section contains all movie related settings for tinyMediaManager
* [[TV show section|TV Show Settings]] This section contains all TV show related settings for tinyMediaManager

You only need to head over to the desired module settings (Movies or TV shows - the buttons at the top) and add a new data source (= folder on the hard disk or network where the video files resist) in the "General" tab of the module. You may adapt the scraper settings (Scraper tab) to your preferred meta data provider and language. Close the settings dialog and you are ready to import the media into tinyMediaManager.

<a name="import" />

## Import media into tinyMediaManager

After adding some data sources, you can let tinyMediaManager search these folders for new media by clicking the button "Update data sources" ![](images/refresh.png) in the main view. This process will take some time until tinyMediaManager analyzes the whole folder structure beneath the data source and adds all found content to its internal database. On the lower right of the tinyMediaManager window you see the progress of importing media.

After this step has been finished, tinyMediaManager has imported every found media file along with detected meta data like title and exisiting NFO files into its internal database.

<a name="scrape" />

## Search and Scrape meta data from the internet

After importing media into your database, you can search for meta data in the internet by selecting movies/TV shows and clicking the "Search and scrape" ![](images/search.png) button in the main view. After that a new dialog opens, a search with the preferred media scraper is triggered and the search result is presented to the user.

After selecting the right search result, tinyMediaManager downloads all preferred meta data (select able in the lower part of the search window) and stores it into the NFO files for further usage (importing it into a media center tool like Kodi, or re-importing it into tinyMediaManager after a new set up).

---
These are the basic steps to do maintain your media library with tinyMediaManager. There are numerous different settings and action which will assist you in different scenarios - just give them a try.

[1]: http://www.java.com
[2]: /docs/installation
[3]: https://adoptopenjdk.net
[4]: https://adoptopenjdk.net/releases.html?variant=openjdk11&jvmVariant=openj9#x64_win
[5]: https://adoptopenjdk.net/releases.html?variant=openjdk11&jvmVariant=openj9#x64_mac
