---
layout: single
permalink: "/docs/movies/multi-version"
title: "Multi-version Movies"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

## The problem

Having multiple versions of the same movie (as 2D vs. 3D, cinematic vs. extended cut, 720p vs. 4k, ...) is fully supported by the main media centers like Kodi, Plex and Emby. Every media center supports different aspects of this topic. At the time of writing this is:

* **Kodi** does not support multiple versions of the same movie at all

* **Plex** only supports different video formats of the same movie (720p vs. 4k). There is also a note in their wiki that this will not support different "versions" of the same movie ([Source](https://support.plex.tv/articles/200381043-multi-version-movies/)):  
> **Note**: This feature is not intended for situations where you might have, for instance, both a “theatrical” and “director’s cut” version of a movie. Likewise, it is not appropriate for 2D vs 3D versions. Those situations are best handled by Splitting the movie into separate library items.

* **Emby** has the best support for multi version movies with one drawback: you have to follow a strict naming scheme. Every video file must start with the same name as the movie folder followed by "space, dash, space, version name". E.g. `300 (2006) - 1080p.mkv`. ([Source](https://support.emby.media/support/solutions/articles/44001159102-movie-naming))

We could implement the Emby style, but there is more to take care about in the media manager: at the time of scanning your library you won't have a "nice" folder-/filename scheme (such as Emby expects to be able to group the movie versions together). Furthermore tinyMediaManager can hardly distinguish between a folder with multiple versions of the same movie (multi version movie folder) and a folder with multiple different movies (multi movie folder). But this is not the end - we also have to handle "extras" (like external subtitles) for each version accordingly. There is also **no** hint how to handle DVD (VIDEO_TS) and BD (BDMV) structures in such a scheme.

With that in mind we decided _against_ implementing such a feature (for now). **If** there is a good solution from the media centers we might re-evaluate the situation.

## Possible solutions

There are some solutions how you can still have multiple versions of the same movie in your library:

1. Put all your versions in separate folders. This has the benefit that they are separated from each other and there is no interference with the other versions. This approach also has full support for all types of movies (disc images, DVD/BR folder structures, splitted files, ...). To separate the movies in the renamer, you have to adopt your folder pattern to any _unique_ pattern for each movie. This heavily depends on _how_ you set up your library. With this approach you will have all versions available inside tinyMediaManager (UI, search, renamer, scraping, ...).
```
H:\movies
    300 (2006) - 1080p
        300 (2006) - 1080p.mkv
        movie.nfo
        fanart.jpg
        poster.jpg
        sample
            sample.avi
    300 (2006) - 4k
        300 (2006) - 4k.mkv
        movie.nfo
        fanart.jpg
        poster.jpg
    300 (2006) - extended cut
        extended cut.avi
        movie.nfo
        fanart.jpg
        poster.jpg
    ...
```

2. Put your _main_ version into its own folder and all other versions (including extra files like subtitles) into a subfolder called `extras`. tinyMediaManager will ignore all files in `extra` so they are untouched. With this approach you don't need to adopt the renamer pattern and you only have the _main_ version of the movie inside tinyMediaManager.
```
H:\movies
    300 (2006)
        300 (2006).mkv
        movie.nfo
        fanart.jpg
        poster.jpg
        extras
            300 (2006) - 4k.mkv
            300 (2006) - extended cut.avi
    ...
```

3. Put all your versions into one folder (somehow the Emby approach). This will create a multi movie folder and tinyMediaManager automatically applies the naming scheme conform to multi movie folder (so every movie get its associated files with the right postfixes rather than neutral files like poster.jpg). This will make it possible to create an Emby like structure (but with every movie have its own artwork) and to be tinyMediaManager conform. **BUT BE AWARE - DO NOT USE FOLDER PATTERNS WITH THAT APPROACH** (because starting with renaming the second version from this movie, tinyMediaManager detects an existing movie folder and aborts renaming because it could overwrite existing files). Using this approach you also have all your versions inside tinyMediaManager.
```
H:\movies
    300 (2006)
        300 (2006) - 1080p.mkv
        300 (2006) - 1080p.nfo
        300 (2006) - 1080p-fanart.jpg
        300 (2006) - 1080p-poster.jpg
        300 (2006) - 4k.mkv
        300 (2006) - 4k.nfo
        300 (2006) - 4k-fanart.jpg
        300 (2006) - 4k-poster.jpg
        300 (2006) - extended cut.mkv
        300 (2006) - extended cut.nfo
        300 (2006) - extended cut-fanart.jpg
        300 (2006) - extended cut-poster.jpg
    ...
```

From the tinyMediaManager perspective approach #1 would fit best for overall consistency/flexibility - and is already fully supported by tinyMediaManager.
