---
layout: single
permalink: "/docs/movies/renamer"
title: "Movie Renamer"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

tinyMediaManager offers a powerful renamer to rename your movies and all associated files to your desired folder-/filenames. While there is almost nothing you can't do with the renamer, it has still one big restriction: you can only rename the movies inside its own data source. Renaming it to a destination which is not inside the own data source is not supported.

## Renamer Pattern ##

* **Folder name** and **Filename**: Choose the desired folder name and filename for renaming. Here you can enter fixed parts of the name and dynamic ones. You will find a list of all available tokens for building up dynamic names beneath the settings along with examples of your media library. With leaving the folder name/filename empty, the renamer will skip the name generation for the empty part.

The renamer is built on [JMTE](/docs/jmte/) and can profit from all options you can have in JMTE. For easier usage, we have added some preset tokens (see table below). Nevertheless you can also access every field from the movie just by using the full syntax like `${movie.title}`. **Note**: if the preset token already contains a renderer (like `${filesize}` -> `${movie.videoFilesize;filesize}`), you cannot add another renderer. In this case you need to switch to the full syntax.

### Available tokens ###

| Token | Description | Example |
| --- | --- | --- |
| ${title}<br>${movie.title} | Title |	One Hundred and One Dalmatians |
| ${originalTitle}<br>${movie.originalTitle} | Original title | One Hundred and One Dalmatians |
| ${title[0]}<br>${movie.title[0]} | First character of the title |	O |
| ${title;first}<br>${movie.title;first} | First letter or # (for non letters) | O |
| ${title[0,2]}<br>${movie.title[0,2]} | First two characters of the title | On |
| ${titleSortable}<br>${movie.titleSortable} | Title sorting (e.g. Matrix, The) | One Hundred and One Dalmatians |
| ${originalFilename}<br>${movie.originalFilename} | Filename of the movie at the first import | 101.dalmatians.avi |
| ${year}<br>${movie.year} | Year | 1961 |
| ${decadeLong}<br>${movie.decadeLong} | Decade long | 1960-1969 |
| ${decadeShort}<br>${movie.decadeShort} | Decade short | 1960s |
| ${releaseDate}<br>${movie.releaseDate} | Release date | 1961-04-25 |
| ${movieSet.title}	| Movieset title | 101 Dalmatians (Animated) Collection |
| ${movieSet.titleSortable}	| Movieset title (sort title) |	101 Dalmatians (Animated) Collection |
| ${rating}<br>${movie.rating.rating}	| Rating | 6.7 |
| ${imdb}<br>${movie.imdbId} | IMDb Id |	tt0055254 |
| ${tmdb}<br>${movie.tmdbId} | TMDB Id |	1234 |
| ${certification}<br>${movie.certification} | Certification | G |
| ${directors[0].name}<br>${movie.directors[0].name} | Name of first director |	Clyde Geronimi |
| ${genres[0]}<br>${movie.genres[0]} | First genre (localized name if available) |	Adventure |
| ${genres[0].name}<br>${movie.genres[0].name} | First genre (English) | Adventure |
| ${genresAsString}<br>${movie.genresAsString} | All genres separated by ', ' |	Adventure, Animation, Comedy, Family |
| ${tags[0]}<br>${movie.tags[0]} | First tag | Tag1 |
| ${language}<br>${movie.spokenLanguages}	| Language |	en |
| ${videoResolution}<br>${movie.mediaInfoVideoResolution}	| Video resolution (e.g. 1280x720) | 1920x1080 |
| ${aspectRatio}<br>${movie.mediaInfoAspectRatioAsString} | Aspect ratio (e.g. 178) |	240 |
| ${aspectRatio2}<br>${movie.mediaInfoAspectRatio2AsString} | 2nd aspect ratio (e.g. 178, only for multiformat) | 178 |
| ${videoCodec}<br>${movie.mediaInfoVideoCodec}	| Video codec (e.g. H.264) | h264 |
| ${videoFormat}<br>${movie.mediaInfoVideoFormat}	| Video format (e.g. 720p) | 1080p |
| ${videoBitDepth}<br>${movie.mediaInfoVideoBitDepth}	| Video bit depth (e.g. 8 / 10) | 8 |
| ${videoBitRate}<br>${movie.mediaInfoVideoBitrate;bitrate} | Video bitrate (e.g. 10.4 Mbps) | 10.4 Mbps |
| ${audioCodec}<br>${movie.mediaInfoAudioCodec}	| Audio codec of the default/first stream (e.g. AC3) | AC3 |
| ${audioCodecList}<br>${movie.mediaInfoAudioCodecList}	| Array of all audio codecs (e.g. [AC3, MP3]) | [AC3, MP3] |
| ${audioCodecsAsString}<br>${movie.mediaInfoAudioCodecList;array} | List of all audio codecs (e.g. AC3, MP3) | AC3, MP3 |
| ${audioChannels}<br>${movie.mediaInfoAudioChannels}	| Audio channels of the default/first stream (e.g. 6ch) |	6ch |
| ${audioChannelList}<br>${movie.mediaInfoAudioChannelList}	| Array of all audio channels (e.g. [6ch, 2ch]) |	[6ch, 2ch] |
| ${audioChannelsAsString}<br>${movie.mediaInfoAudioChannelList;array} | List of all audio channels (e.g. 6ch, 2ch) | 6ch, 2ch |
| ${audioLanguage}<br>${movie.mediaInfoAudioLanguage}	| Audio language of the default/first steam (e.g. EN) |	EN |
| ${audioLanguageList}<br>${movie.mediaInfoAudioLanguageList}	| Array of all audio languages (e.g. [EN, DE]) |	[EN, DE] |
| ${audioLanguagesAsString}<br>${movie.mediaInfoAudioLanguageList;array} | List of all audio languages (e.g. EN, DE) | EN, DE |
| ${subtitleLanguageList}<br>${movie.mediaInfoSubtitleLanguageList} |	Array of all subtitle languages (e.g. [EN, DE]) |[en, de] |
| ${subtitleLanguagesAsString}<br>${movie.mediaInfoSubtitleLanguageList;array} |	List of all subtitle 
languages (e.g. EN, DE) |	en, de |
| ${mediaSource}<br>${movie.mediaSource} | Media source (DVD etc.) | BluRay |
| ${3Dformat}<br>${movie.video3DFormat}	| 3D tag (3D SBS/TAB) | 3D |
| ${hdr}<br>${movie.videoHDRFormat}	| High Dynamic Range (HDR) | HDR |
| ${hdrformat}<br>${movie.videoHDRFormat} |	High Dynamic Range (HDR) Format	| HDR 10 |
| ${edition}<br>${movie.edition} | Edition | Directors Cut |
| ${filesize}<br>${movie.videoFilesize;filesize} | File size in GB | 1.05 G |
| ${parent}<br>${movie.parent}| Path between data source and parent folder |  Disney |
| ${note}<br>${movie.note} | Movie note | My personal note |  
| ${movieSetIndex}<br>${movie;indexOfMovieSet} | Index in the movie set | 2 |  
| ${movieSetIndex2}<br>${movie;indexOfMovieSetWithDummy} | Index in the movie set (respecting missing movies) | 3 |  

Some hints for the renamer:

* If there wouldn't be a change (old and new file-/folder name are identical), the renamer skips renaming
* All supported files within the movie folder (NFO, poster, fanart, …) will be renamed as configured in the settings

### Renderers ###

You can also use some pre-built renderers to modify the result; you cannot use the preset token for the renderers to work - you need to use the full syntax:

* _UPPER_ case renderer `upper`: `${movie.title;upper}`
* _lower_ case renderer `lower`: `${movie.title;lower}`
* _Title_ case renderer `title`: `${movie.title;title}`
* First character upper case renderer `first`: `${movie.title;first}`
* Array renderer `array`: `${movie.genres;array}` would render all genres separated by a comma
* Unique array renderer `uniqueArray`: `${movie.mediaInfoAudioLanguageList;uniqueArray}` would render all unique audio languages separated by a comma
* Date format renderer `date`: `${movie.releaseDate;date(yyyy-MM-dd)}` would cause the release date to be formatted with a renderer named "date". This named renderer would then be passed the format "yyyy-MM-dd" along with the variable to be rendered.
* Number format renderer `number`: `${movie.ratings.tomatometerallcritics.rating;number(%.0f)}` would format the Rotten Tomatoes rating (internally a float) as an integer. You need to know which number type is returned from the field - after that you can use any supported [Java String.format token](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Formatter.html).
* File name renderer `filename`: `${movie;filename(escape=true)}` would a valid file name for the movie (according to your renamer settings) - useful for linking between master and detail pages. The parameter `escape=true` can be used to create an escaped url
* File size renderer `filesize`: `${movie;filesize(G)}` to format the video filesize to the unit you want (K, KB, KiB, M, MB, MiB, G, GB, GiB are supported. G is the default if you don't add a unit)
* Replacement renderer `replace`: `${movie.title;replace(umlauts.csv)}` would load replacement tokens from the file `/data/umlauts.csv` and to a search and replace of the token for every line in the csv. The csv needs to be comma separated and only the comma itself needs to be quoted with double quotes. More info at the [corresponding blog post](/blog/jmte-replacement-renderer/). If you just want to replace single token, you can also put it inline into the replacement renderer: `${episode.title;replace(x264,h264)}`
* Bitrate renderer `bitrate`: `${movie.mediaInfoVideoBitrate;bitrate(Mbps)}` to format the bitrate to the unit you want (k, kb, kbps, M, Mb, Mbps are supported. Mbps is the default if you don't add a unit)
* Chaining renderer `chain`: this renderer can be used to chain the output through multiple renderers. E.g. `${movie.title;chain(replace(umlauts.csv);lower)}`

## Advanced Options ##

* **Replace spaces in folder name** and **Replace spaces in filename**: You can replace all spaces with either underscores, dots or dashes by activation this option.
* **Replace colons with**: Since colons are illegal characters in folder names and filenames, tinyMediaManager offers you to choose how they should be replaced.
* **Replace non ASCII characters with their basic ASCII equivalents**: Some file systems might need to have ASCII conform file names - by activating this option tinyMediaManager tries to convert non ASCII characters into a similar ASCII character (e.g. Ä - Ae; ß - ss, ...)
* **First character number replacement**: If you use the renderer `;first` (like in `${title;first}`) and the first character would be a digit, replace the digit with the given character.
* **Enable movie set tokens also for movie sets containing only one movie**: Unless this is activated, the renamer only creates a movie set folder if there is more than one movie in the set.
* **Remove all other NFOs**: Remove all non tinyMediaManager NFOs when renaming (clean up)   