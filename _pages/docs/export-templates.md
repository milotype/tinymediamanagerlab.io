---
layout: single
permalink: "/docs/exporttemplates"
title: "Export Templates"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

# Export Templates

tinyMediaManager uses the Java Minimal Template Engine ([JMTE][1]) to construct the exported page. You will find the JMTE Language reference in the [docs][5].

## Creating templates
If you want to edit or create a new template make a copy of the template folder you would like to build upon. The template folders are in the tinyMediaManager folder (or inside the tinyMediaManager.app -> `/Contents/Resources/Java/templates` on macOS). **Do NOT edit the default templates as they will be overwritten each time you start tinyMediaManager and you will lose your changes!**

If you create a nice template and want us to ship it to all users, contact us at [GitLab][4], the [Kodi forums][2] or [reddit][3] and we will review it for distribution with the program.  
You can also send plain HTML templates; we would integrate the functionality for you ;)

## Template setup
Templates rely on three files to export successfully. All other files you create will also be exported, retaining their directory structure, when the page is built by tMM; this allows for the inclusion of style sheets, images and scripts.
* **template.conf** - This configuration file tells tMM where to find the other required files.
* **list.jmte** - Template for creating a list of your movies/shows (maybe with links to detail pages)
* **detail.jmte** - Template for detailed information of one movie or show. detail.jmte is required only if you want tMM to build individual <movie>.xxx files for inclusion into index.html either through an .ajax() call or iframe.
* **episode.jmte** - episode.jmte is required only if you want tMM to build individual <episode>.xxx files for inclusion into index.html/detail.html either through an .ajax() call or iframe.

Each template must be in its own directory and include a template.conf file. The contents of template.conf must include:
* **name=\<name of template\>** - The name that will display to the user when exporting through the UI.
* **type={movie, tv_show}** - Currently only movie/tv show templates are supported.
* **list=\<path to list.jmte\>** - (default: list.jmte) This is the template which will be used to build index.html or movielist.xml/csv.
* **detail=\<path to detail.jmte\>** - (default: detail.jmte) Remove this line if you do not require individual \<movie\>.html pages.
* **episode<path to episode.jmte>** - (default: episode.jmte) Only for TV show exporting! This is the template for episode data export.
* **extension={html|xml|csv}** - (default: html) This is the format tMM will export.
* **description=\<text\>** - Write a short description that will print in the tMM exporter UI. Newlines (\n) should be used to insert paragraph breaks.
* **url=\<url to homepage\>** - The URL to the page that hosts this template or to the author's homepage. Remove this line if you have neither.

Using the above information write your template.conf file. It may resemble this example:
```properties
    name=Jelly is Delicious
    type=movie
    list=list.jmte
    detail=detail.jmte
    extension=html
    description=Created by William Shatner\n\nThis template has jelly in its gears.
    url=https://github.com/TheShatner/jelly_template
```

list.jmte and detail.jmte are HTML pages. The JMTE syntax is used to insert variables like movie name, cast, genre and file information. All of the variables are stored in the list array movies. To access each movies' variables you must itterate over the entire list array.

## Accessing data
In the following code the list array movies is iterated over. For each movie entry we assign the variable movie to hold its details and append the name of a variable to print individual attributes.
```html
    <div class="movie details">
    ${foreach movies movie}
        <span class="bold">Title</span>: ${movie.name}
        <span class="bold">Year</span>: ${movie.year}
    ${end}
    </div>
```
As you can see, the name variable in ${movie.name} tells JMTE to print the name of the movie. The variable name is a string, but some movie variables are also list arrays. Print the list array genres with the following code:
```html
    ${foreach movies movie}
        ${movie.name}
        <span class="genreList">
        ${foreach movie.genres genre , } // " , " comma is used here as genre seperator
            ${genre}
        ${end}
        </span>
    ${end}
```    
In this example we iterated over the movies list array like in the previous example. Then, from within the first foreach loop, we iterated over the genres list array and printed them. We told JMTE to separate each entry with a comma by putting a comma at the end of the foreach instance.

To access values in a map, you can simply use map key like the variable (to access a key from type enum, you have to use upper case notation)
```html
    ${foreach movies movie}
        <img src="${movie.artworkUrls.POSTER(../nopicture.gif)}" />
    ${end}     
```
In this example we exported the url of the first poster with jmte and also provided a fallback `(../nopicture.gif)` if there is no poster.

## Exporting images
You can either embed image urls (see the example above) into your templates or export the images from your library.

**Be aware**: Image urls are not always available. Whenever you scrape a movie/episode in tinyMediaManager we store the image url in our internal database. But if you import existing objects into tinyMediaManager (via NFOs), there are no more urls available and your export template could get incomplete.

We've added a feature to trigger an export of images (with or without scaling) along the meta data export. In the following example you see how a movie poster can be exported via the template:
```html
    ${foreach movies movie}
        <img src="../${movie;copyArtwork(type=POSTER, thumb=true, width=180, destination=images, fallback=../nopicture.gif)}" />
    ${end}    
```
To initiate the export process you only need `${movie;copyArtwork(type=...)}`. All other options are optional.

As you see there are some options for exporting available:
* `type=...`: select which artwork type you want to export:
  * POSTER
  * FANART
  * BANNER
  * CLEARART
  * DISCART
  * LOGO
  * CLEARLOGO
  * THUMB
  * CHARACTERART
  * KEYART
  * SEASON_POSTER
  * SEASON_BANNER
  * SEASON_THUMB
* `thumb=...`: rescale the image to a thumbnail? Valid options are `true` and `false`. Default is `false`
* `width=...`: scale to the desired width when creating a thumbnail. Valid is any number. Default: `150`
* `destination=...`: the destination folder where to export the images to. This name is relative to the chosen export folder (e.g. you want to export to `H:\export` and have `img` as destination, all images will be exported to `H:\export\img`). Default: `images`
* `default=../nopicture.gif`: this image will be taken if the desired image can not be exported for this movie
* `escape=...`: set `true` to escape the url to the copied artwork

## Available data

You will find a list of available data for your export templates in the [JMTE docs](/docs/jmte/)

## Available renderers

### Movies

* _UPPER_ case renderer `upper`: `${movie.title;upper}`
* _lower_ case renderer `lower`: `${movie.title;lower}`
* _Title_ case renderer `title`: `${movie.title;title}`
* First character upper case renderer `first`: `${movie.title;first}`
* Array renderer `array`: `${movie.genres;array}` would render all genres separated by a comma
* Date format renderer `date`: `${movie.releaseDate;date(yyyy-MM-dd)}` would cause the release date to be formatted with a renderer named "date". This named renderer would then be passed the format "yyyy-MM-dd" along with the variable to be rendered.
* Number format renderer `number`: `${movie.ratings.tomatometerallcritics.rating;number(%.0f)}` would format the Rotten Tomatoes rating (internally a float) as an integer. You need to know which number type is returned from the field - after that you can use any supported [Java String.format token](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Formatter.html).
* File name renderer `filename`: `${movie;filename(escape=true)}` would a valid file name for the movie (according to your renamer settings) - useful for linking between master and detail pages. The parameter `escape=true` can be used to create an escaped url
* File size renderer `filesize`: `${movie;filesize(G)}` to format the video filesize to the unit you want (K, KB, KiB, M, MB, MiB, G, GB, GiB are supported. G is the default if you don't add a unit)
* Replacement renderer `replace`: `${movie.title;replace(umlauts.csv)}` would load replacement tokens from the file `/data/umlauts.csv` and to a search and replace of the token for every line in the csv. The csv needs to be comma separated and only the comma itself needs to be quoted with double quotes. More info at the [corresponding blog post](/blog/jmte-replacement-renderer/). If you just want to replace single token, you can also put it inline into the replacement renderer: `${episode.title;replace(x264,h264)}`
* Bitrate renderer `bitrate`: `${movie.mediaInfoVideoBitrate;bitrate(Mbps)}` to format the bitrate to the unit you want (k, kb, kbps, M, Mb, Mbps are supported. Mbps is the default if you don't add a unit)
* Chaining renderer `chain`: this renderer can be used to chain the output through multiple renderers. E.g. `${movie.title;chain(replace(umlauts.csv);lower)}`
* Artwork copy renderer `copyArtwork`: as described above 

### TV shows

* _UPPER_ case renderer `upper`: `${tvShow.title;upper}`
* _lower_ case renderer `lower`: `${tvShow.title;lower}`
* _Title_ case renderer `title`: `${tvShow.title;title}`
* First character upper case renderer `first`: `${tvShow.title;first}`
* Array renderer `array`: `${tvShow.genres;array}` would render all genres separated by a comma
* Date format renderer `date`: `${episode.firstAired;date(yyyy-MM-dd)}` would cause the aired date to be formatted with a renderer named "date". This named renderer would then be passed the format "yyyy-MM-dd" along with the variable to be rendered.
* Number format renderer `number`: `${tvShow.ratings.imdb.rating;number(%.0f)}` would format the IMDb rating (internally a float) as an integer. You need to know which number type is returned from the field - after that you can use any supported [Java String.format token](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Formatter.html).
* File size renderer `filesize`: `${episode;filesize(G)}` to format the video filesize to the unit you want (K, KB, KiB, M, MB, MiB, G, GB, GiB are supported. G is the default if you don't add a unit)
* Replacement renderer `replace`: `${episode.title;replace(umlauts.csv)}` would load replacement tokens from the file `/data/umlauts.csv` and to a search and replace of the token for every line in the csv. The csv needs to be comma separated and only the comma itself needs to be quoted with double quotes. More info at the [corresponding blog post](/blog/jmte-replacement-renderer/). If you just want to replace single token, you can also put it inline into the replacement renderer: `${episode.title;replace(x264,h264)}`
* Bitrate renderer `bitrate`: `${episode.mediaInfoVideoBitrate;bitrate(Mbps)}` to format the bitrate to the unit you want (k, kb, kbps, M, Mb, Mbps are supported. Mbps is the default if you don't add a unit)
* Chaining renderer `chain`: this renderer can be used to chain the output through multiple renderers. E.g. `${episode.title;chain(replace(umlauts.csv);lower)}`
* Artwork copy renderer `copyArtwork`: as described above 

[1]: https://github.com/DJCordhose/jmte
[2]: https://forum.kodi.tv/forumdisplay.php?fid=204
[3]: https://www.reddit.com/r/tinyMediaManager/
[4]: https://gitlab.com/tinyMediaManager/tinyMediaManager/
[5]: /docs/jmte
