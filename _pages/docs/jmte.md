---
layout: single
permalink: "/docs/jmte"
title: "JMTE Template Engine"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

# Java Minimal Template Engine #

tinyMediaManager uses the Java Minimal Template Engine (JMTE) to perform "object to text" transformations which are used in

* the renamer (movies & TV shows)
* export templates

JMTE has a flexible syntax to achieve _almost_ everything you like. The language is really simple and can be learned in no time. It is all about having a text with small island of expressions that expand to text or control the expansion of text fragments. Those expressions are enclosed in special characters which are freely configurable but are by default

* `${` for the start of an expression and
*	`}` for its end

## Replacing variables ##

The simplest form of an expression is to expand an entry contained in the model to a string: `${title}`

If your model contains complex objects and you want to expand a certain property or field you can use the "." notation to navigate to it like e.g. `${movie.title}`.

## Renderer information ##

How a variable is eventually converted to a string is determined by renderers. There are renderers which are automatically called based on the type of the variable. However, there are also so called "named renderers" which you can
address by passing their name along with the name of the variable. You do this by adding a semicolon and the name of the renderer to the variable name. You can even pass format information in brackets.

For example this

```
${movie.releaseDate;date(yyyy-MM-dd)}
```

would cause the variable called `movie.releaseDate` to be formatted with a renderer named `date`. This named renderer would then be passed the format `yyyy-MM-dd` along with the variable to be rendered. Named renderers know which types they support and are responsible to do the necessary conversion for them.

## Conditional expansion ##

Sometimes you might want to display certain text or expand certain expression only when certain conditions hold. The minimal template engine provides you with an if-statement to do so.

```
${if condition}
  text displayed if condition holds
${else}
  text displayed if condition does not hold
${end}
```

The else-part is optional and the whole conditional block has to be terminated by the end-expression. If-expressions can be nested and conditions can be negated using the "!"-operator. You can also compare to a string constant using the "="-operator.

```
${if ! media.type = "VIDEO"}
  text displayed if media.type is NOT "VIDEO"
${else}
  text displayed if media.type is "VIDEO"
${end}
```

There are no other logical operators, i.e. you can not combine conditions using `and` or `or`. The reason is that this is a minimal language, expressions can be complex and conditions can be constructed in the Java model. You can either have a condition directly inside the model or you can build a processor that computes it and add it to the model.

The condition can be any object. It evaluates using these rules

* if the object is `null` or calling `toString` on the object returns the empty string or `false` the	condition evaluates to `false`
* if the object is a boolean, the condition evaluates to the value of that boolean
* if the object is a map, a collection, an array or any iterable the condition evaluates to `true` if they contain any elements
*	otherwise the condition evaluates to `true`

## Conditional shortcuts ###

For convenience reasons there are some shortcuts for the most commonly used conditional statements. The first shortcut works by supplying a default constant value which will be used in case the variable is undefined. You can specify such a default value in brackets. E.g.

```
${movie.title(unknown)}
```

would be equivalent to

```
${if movie.title}
  ${movie.title}
${else}
  unknown
${end}
```

The second shortcut works by wrapping a value, but only if it is defined. You can wrap a value by adding a prefix and a suffix separated by commas. This means

```
${<h1>,movie.title,</h1>}
```

is essentially the same as

```html
${if movie.title}
  <h1>${movie.title}</h1>
${end}
```

To avoid ambiguities, both prefix and suffix must be supplied or none at all, but they can also be the empty string.

## Loops (expanding more than one item) ##

If you want to expand all elements of a container you can use the foreach-expression.

```
${foreach container item}
  ${item}
${end}
```

In this example variable `item` is set to each element of the container and the body - i.e. the part between the foreach- and the end-expression - is expanded for each item in the container. The container can be anything that implements `java.lang.Iterable` or anything that implements `java.util.Map` or any array. If the container is a map we iterate over its entry set and the items will be of type `Map.Entry`. This means you can access its parts using `${item.key}` and `${item.value}`.

Loops can be nested and can also contain all kinds of other expressions. You are not able to specify a step or a start of end index.

### Special variables ###

In certain cases you might want to change the way an item in a container shall be displayed depending on its position inside the container. One example is to have the rows in a table be displayed having an alternating format depending whether their row number is even or odd.

Current list of special variables

* first
* last
* odd
* even
* index

Special variables are suffixed by a `_` followed by the name of the item variable. This means inside a foreach loop that has `item` as the item name there will be the variable `first_item`.

Example:

```html
${foreach items item}
  ${if first_item}
    <em>${item}</em>
  ${else}
    ${item}
  ${end}
${end}
```

### Separators ###

When displaying a list of items you might want to have a separator between each item. This will not work using a normal body as there shall be no additional separator after the last item. You can either solve this using special variables - explained in the previous section - or more easily using a separator. Such a separator is added into the foreach expression like this

```
${foreach container item , }${item}${end}
```

Suppose the container contains the items `item1` and `item2` then the formatted output looks like `item1, item2`.

## Escaping ##

When you want to use the special character sequences literally you will have to escape them using a backslash `\`. You can also escape the backslash by adding a second. In tinyMediaManager and templates this would be

* \{
* \}
* \\

## Regular expression annotation

You can use this regular expression annotation to extract/modify parts of the chosen token. The syntax of the annotation is as follows:

`${@regexp expression source destination}`

where 

* `${@regexp` - opens the annotation for JMTE
* `expression` - is the regular expression. **NOTE**: you cannot use `{}` in the regular expression since those will conflict with JMTE
* `source` - the source token where to take the information from. **ATTENTION**: you need to insert the whole token (and not the short token). E.g. `movie.originalFilename`
* `destination` - a string with placeholders where the found parts of the regular expression will be entered. `$1` will use the findings from group 1, `$2`will use findings from group 2, ...

JMTE uses the Java syntax for regular expression. You can test your regular expression with the online tool [RegexPlanet](https://www.regexplanet.com/advanced/java/index.html).
{: .notice--info}

## Examples

Get the first actor name

```
${actors[0].name}
```

Get a list of all actors (comma separated)

```
${actors;array}
```

Get a list of all actors (separated by your custom param)

```
${actors;array(=^..^=)}
```

Limit the list to 3 actors (starting from first=0)

```
${actors[0,3];array}
```

Limit the list to 3 actors (starting from the second actor, omitting the first)

```
${actors[1,3];array}
```

Chain renderers together, to get a comma-separated list of 3 actors, convert them to uppercase, and replace some umlauts from a file ;)

```
${actors[0,3];chain(array(, );upper;replace(umlauts.csv))}
```

## Available data

In addition to the shortcut tokens of the [movie renamer](/docs/movies/settings/#renamer) and [TV show renamer](/docs/tvshows/settings/#renamer) you are able to access most internal data structures by directly addressing them. For example if you want to address the bitrate of the first audio stream of the main video file in a movie, you have to navigate the following way with JMTE syntax:

```
${movie.mainVideoFile.audioStreams[0].bitrate}
```

The following list contains an automatic generated export of all available fields in the entities used for movies and TV shows:

### Movie ###

Entity name: **Movie**

| Type | Full access | Shortcut |
| --- | --- | --- |
| List\<Person\> | ${movie.actors} | ${actors} |
| Map\<String,MediaFile\> | ${movie.artworkMap} |  | 
| Map\<String,String\> | ${movie.artworkUrls} |  | 
| String | ${movie.certification} | ${certification} |
| String | ${movie.country} |  | 
| String | ${movie.dataSource} |  | 
| Date | ${movie.dateAdded} |  | 
| String | ${movie.dateAddedAsString} |  | 
| Date | ${movie.dateAddedForUi} |  | 
| UUID | ${movie.dbId} |  | 
| String | ${movie.decadeLong} | ${decadeLong} |
| String | ${movie.decadeShort} | ${decadeShort} |
| List\<Person\> | ${movie.directors} | ${directors} |
| String | ${movie.directorsAsString} |  | 
| boolean | ${movie.disc} |  | 
| boolean | ${movie.duplicate} |  | 
| String | ${movie.edition} | ${edition} |
| String | ${movie.editionAsString} |  | 
| List\<String\> | ${movie.extraFanarts} |  | 
| List\<String\> | ${movie.extraThumbs} |  | 
| List\<String\> | ${movie.genres} | ${genres} |
| String | ${movie.genresAsString} | ${genresAsString} |
| Boolean | ${movie.hasImages} |  | 
| Boolean | ${movie.hasMetadata} |  | 
| Boolean | ${movie.hasNfoFile} |  | 
| Boolean | ${movie.hasNote} |  | 
| boolean | ${movie.hasRating} |  | 
| boolean | ${movie.hasSubtitles} |  | 
| Boolean | ${movie.hasTrailer} |  | 
| Map\<String,Object\> | ${movie.ids} |  | 
| List\<MediaFile\> | ${movie.imagesToCache} |  | 
| String | ${movie.imdbId} | ${imdb} |
| String | ${movie.lastScrapeLanguage} |  | 
| String | ${movie.lastScraperId} |  | 
| Date | ${movie.lastWatched} |  | 
| String | ${movie.localizedSpokenLanguages} |  | 
| boolean | ${movie.locked} |  | 
| MediaFile | ${movie.mainDVDVideoFile} |  | 
| MediaFile | ${movie.mainFile} |  | 
| MediaFile | ${movie.mainVideoFile} |  | 
| List\<MediaFile\> | ${movie.mediaFiles} |  | 
| List\<MediaFile\> | ${movie.mediaFilesContainingAudioStreams} |  | 
| List\<MediaFile\> | ${movie.mediaFilesContainingSubtitles} |  | 
| float | ${movie.mediaInfoAspectRatio} |  | 
| Float | ${movie.mediaInfoAspectRatio2} |  | 
| String | ${movie.mediaInfoAspectRatio2AsString} | ${aspectRatio2} |
| String | ${movie.mediaInfoAspectRatioAsString} | ${aspectRatio} |
| List\<String\> | ${movie.mediaInfoAudioChannelList} | ${audioChannelList} |
| String | ${movie.mediaInfoAudioChannels} | ${audioChannels} |
| String | ${movie.mediaInfoAudioCodec} | ${audioCodec} |
| String | ${movie.mediaInfoAudioCodecAndChannels} |  | 
| List\<String\> | ${movie.mediaInfoAudioCodecList} | ${audioCodecList} |
| String | ${movie.mediaInfoAudioLanguage} | ${audioLanguage} |
| List\<String\> | ${movie.mediaInfoAudioLanguageList} | ${audioLanguageList} |
| String | ${movie.mediaInfoContainerFormat} |  | 
| double | ${movie.mediaInfoFrameRate} |  | 
| String | ${movie.mediaInfoSource} |  | 
| List\<String\> | ${movie.mediaInfoSubtitleLanguageList} | ${subtitleLanguageList} |
| int | ${movie.mediaInfoVideoBitDepth} | ${videoBitDepth} |
| int | ${movie.mediaInfoVideoBitrate} |  | 
| String | ${movie.mediaInfoVideoCodec} | ${videoCodec} |
| String | ${movie.mediaInfoVideoFormat} | ${videoFormat} |
| String | ${movie.mediaInfoVideoResolution} | ${videoResolution} |
| String | ${movie.mediaSource} | ${mediaSource} |
| MovieSet | ${movie.movieSet} |  | 
| String | ${movie.movieSetTitle} |  | 
| boolean | ${movie.multiFormat} |  | 
| boolean | ${movie.multiMovieDir} |  | 
| boolean | ${movie.newlyAdded} |  | 
| String | ${movie.note} | ${note} |
| boolean | ${movie.offline} |  | 
| String | ${movie.originalFilename} | ${originalFilename} |
| String | ${movie.originalTitle} | ${originalTitle} |
| String | ${movie.originalTitleSortable} |  | 
| String | ${movie.otherIds} |  | 
| String | ${movie.parent} | ${parent} |
| String | ${movie.path} |  | 
| Path | ${movie.pathNIO} |  | 
| int | ${movie.playcount} |  | 
| String | ${movie.plot} |  | 
| List\<Person\> | ${movie.producers} | ${producers} |
| String | ${movie.productionCompany} | ${productionCompany} |
| List\<String\> | ${movie.productionCompanyAsArray} | ${productionCompanyAsArray} |
| MediaRating | ${movie.rating} |  | 
| Map\<String,MediaRating\> | ${movie.ratings} |  | 
| Date | ${movie.releaseDate} |  | 
| String | ${movie.releaseDateAsString} |  | 
| String | ${movie.releaseDateFormatted} |  | 
| int | ${movie.runtime} |  | 
| int | ${movie.runtimeFromMediaFiles} |  | 
| int | ${movie.runtimeFromMediaFilesInMinutes} |  | 
| int | ${movie.runtimeFromMediaFilesInSeconds} |  | 
| boolean | ${movie.scraped} |  | 
| List\<String\> | ${movie.showlinks} |  | 
| String | ${movie.showlinksAsString} |  | 
| String | ${movie.sortTitle} | ${sorttitle} |
| String | ${movie.spokenLanguages} | ${language} |
| boolean | ${movie.stacked} |  | 
| String | ${movie.tagline} |  | 
| List\<String\> | ${movie.tags} | ${tags} |
| String | ${movie.tagsAsString} |  | 
| String | ${movie.title} | ${title} |
| String | ${movie.titleForUi} |  | 
| String | ${movie.titleSortable} | ${titleSortable} |
| int | ${movie.tmdbId} | ${tmdb} |
| int | ${movie.top250} |  | 
| long | ${movie.totalFilesize} |  | 
| List\<MediaTrailer\> | ${movie.trailer} |  | 
| int | ${movie.traktId} |  | 
| MediaRating | ${movie.userRating} |  | 
| String | ${movie.video3DFormat} | ${3Dformat} |
| String | ${movie.videoBasenameWithoutStacking} |  | 
| List\<MediaFile\> | ${movie.videoFiles} |  | 
| long | ${movie.videoFilesize} |  | 
| String | ${movie.videoHDR} | ${hdr} |
| String | ${movie.videoHDRFormat} | ${hdrformat} |
| boolean | ${movie.videoIn3D} |  | 
| boolean | ${movie.watched} |  | 
| List\<Person\> | ${movie.writers} | ${writers} |
| String | ${movie.writersAsString} |  | 
| int | ${movie.year} | ${year} |

### Movie Set ###

Entity name:  **MovieSet**

| Type | Full access | Shortcut |
| --- | --- | --- |
| Map\<String,MediaFile\> | ${movieSet.artworkMap} |  | 
| Map\<String,String\> | ${movieSet.artworkUrls} |  | 
| String | ${movieSet.dataSource} |  | 
| Date | ${movieSet.dateAdded} |  | 
| String | ${movieSet.dateAddedAsString} |  | 
| Date | ${movieSet.dateAddedForUi} |  | 
| UUID | ${movieSet.dbId} |  | 
| String | ${movieSet.decadeLong} |  | 
| String | ${movieSet.decadeShort} |  | 
| List\<MovieSetMovie\> | ${movieSet.dummyMovies} |  | 
| boolean | ${movieSet.duplicate} |  | 
| List\<String\> | ${movieSet.genres} |  | 
| Map\<String,Object\> | ${movieSet.ids} |  | 
| List\<MediaFile\> | ${movieSet.imagesToCache} |  | 
| String | ${movieSet.lastScrapeLanguage} |  | 
| String | ${movieSet.lastScraperId} |  | 
| boolean | ${movieSet.locked} |  | 
| MediaFile | ${movieSet.mainFile} |  | 
| List\<MediaFile\> | ${movieSet.mediaFiles} |  | 
| List\<Movie\> | ${movieSet.movies} |  | 
| List\<Movie\> | ${movieSet.moviesForDisplay} |  | 
| boolean | ${movieSet.newlyAdded} |  | 
| String | ${movieSet.note} |  | 
| String | ${movieSet.originalFilename} |  | 
| String | ${movieSet.originalTitle} |  | 
| String | ${movieSet.parent} |  | 
| String | ${movieSet.path} |  | 
| Path | ${movieSet.pathNIO} |  | 
| String | ${movieSet.plot} |  | 
| String | ${movieSet.productionCompany} |  | 
| List\<String\> | ${movieSet.productionCompanyAsArray} |  | 
| MediaRating | ${movieSet.rating} |  | 
| Map\<String,MediaRating\> | ${movieSet.ratings} |  | 
| Date | ${movieSet.releaseDate} |  | 
| boolean | ${movieSet.scraped} |  | 
| List\<String\> | ${movieSet.tags} |  | 
| String | ${movieSet.tagsAsString} |  | 
| String | ${movieSet.title} |  | 
| String | ${movieSet.titleForStorage} |  | 
| String | ${movieSet.titleSortable} |  | 
| int | ${movieSet.tmdbId} |  | 
| long | ${movieSet.totalFilesize} |  | 
| MediaRating | ${movieSet.userRating} |  | 
| int | ${movieSet.year} |  | 
| String | ${movieSet.years} |  | 

### TV Show ###

Entity name: **TvShow**

| Type | Full access | Shortcut |
| --- | --- | --- |
| List\<Person\> | ${tvShow.actors} |  | 
| Map\<String,MediaFile\> | ${tvShow.artworkMap} |  | 
| Map\<String,String\> | ${tvShow.artworkUrls} |  | 
| String | ${tvShow.certification} |  | 
| String | ${tvShow.country} |  | 
| String | ${tvShow.dataSource} |  | 
| Date | ${tvShow.dateAdded} |  | 
| String | ${tvShow.dateAddedAsString} |  | 
| Date | ${tvShow.dateAddedForUi} |  | 
| UUID | ${tvShow.dbId} |  | 
| String | ${tvShow.decadeLong} |  | 
| String | ${tvShow.decadeShort} |  | 
| int | ${tvShow.dummyEpisodeCount} |  | 
| List\<TvShowEpisode\> | ${tvShow.dummyEpisodes} |  | 
| boolean | ${tvShow.duplicate} |  | 
| int | ${tvShow.episodeCount} |  | 
| List\<TvShowEpisode\> | ${tvShow.episodes} |  | 
| List\<TvShowEpisode\> | ${tvShow.episodesForDisplay} |  | 
| List\<MediaFile\> | ${tvShow.episodesMediaFiles} |  | 
| List\<TvShowEpisode\> | ${tvShow.episodesToScrape} |  | 
| List\<String\> | ${tvShow.extraFanartUrls} |  | 
| Date | ${tvShow.firstAired} |  | 
| String | ${tvShow.firstAiredAsString} |  | 
| String | ${tvShow.firstAiredFormatted} |  | 
| List\<String\> | ${tvShow.genres} | ${showGenres} |
| String | ${tvShow.genresAsString} | ${showGenresAsString} |
| Boolean | ${tvShow.hasEpisodeMetadata} |  | 
| Boolean | ${tvShow.hasMusicTheme} |  | 
| Boolean | ${tvShow.hasNfoFile} |  | 
| Boolean | ${tvShow.hasNote} |  | 
| Boolean | ${tvShow.hasSeasonAndEpisodeImages} |  | 
| Boolean | ${tvShow.hasTrailer} |  | 
| Map\<String,Object\> | ${tvShow.ids} |  | 
| List\<MediaFile\> | ${tvShow.imagesToCache} |  | 
| String | ${tvShow.imdbId} | ${showImdb} |
| String | ${tvShow.lastScrapeLanguage} |  | 
| String | ${tvShow.lastScraperId} |  | 
| Date | ${tvShow.lastWatched} |  | 
| boolean | ${tvShow.locked} |  | 
| MediaFile | ${tvShow.mainFile} |  | 
| MediaFile | ${tvShow.mainVideoFile} |  | 
| List\<MediaFile\> | ${tvShow.mediaFiles} |  | 
| float | ${tvShow.mediaInfoAspectRatio} |  | 
| Float | ${tvShow.mediaInfoAspectRatio2} |  | 
| List\<String\> | ${tvShow.mediaInfoAudioChannelList} |  | 
| String | ${tvShow.mediaInfoAudioChannels} |  | 
| String | ${tvShow.mediaInfoAudioCodec} |  | 
| List\<String\> | ${tvShow.mediaInfoAudioCodecList} |  | 
| String | ${tvShow.mediaInfoAudioLanguage} |  | 
| List\<String\> | ${tvShow.mediaInfoAudioLanguageList} |  | 
| String | ${tvShow.mediaInfoContainerFormat} |  | 
| double | ${tvShow.mediaInfoFrameRate} |  | 
| String | ${tvShow.mediaInfoSource} |  | 
| List\<String\> | ${tvShow.mediaInfoSubtitleLanguageList} |  | 
| int | ${tvShow.mediaInfoVideoBitDepth} |  | 
| String | ${tvShow.mediaInfoVideoCodec} |  | 
| String | ${tvShow.mediaInfoVideoFormat} |  | 
| String | ${tvShow.mediaInfoVideoResolution} |  | 
| boolean | ${tvShow.newlyAdded} |  | 
| String | ${tvShow.note} | ${showNote} |
| String | ${tvShow.originalFilename} |  | 
| String | ${tvShow.originalTitle} | ${showOriginalTitle} |
| String | ${tvShow.otherIds} |  | 
| String | ${tvShow.parent} | ${parent} |
| String | ${tvShow.path} |  | 
| Path | ${tvShow.pathNIO} |  | 
| String | ${tvShow.plot} |  | 
| String | ${tvShow.productionCompany} |  | 
| List\<String\> | ${tvShow.productionCompanyAsArray} |  | 
| MediaRating | ${tvShow.rating} |  | 
| Map\<String,MediaRating\> | ${tvShow.ratings} |  | 
| Date | ${tvShow.releaseDate} |  | 
| int | ${tvShow.runtime} |  | 
| boolean | ${tvShow.scraped} |  | 
| int | ${tvShow.seasonCount} |  | 
| Map\<Integer,String\> | ${tvShow.seasonTitles} |  | 
| List\<TvShowSeason\> | ${tvShow.seasons} |  | 
| String | ${tvShow.sortTitle} |  | 
| String | ${tvShow.status} | ${showStatus} |
| List\<String\> | ${tvShow.tags} | ${showTags} |
| String | ${tvShow.tagsAsString} |  | 
| String | ${tvShow.title} | ${showTitle} |
| String | ${tvShow.titleSortable} | ${showTitleSortable} |
| int | ${tvShow.tmdbId} | ${showTmdb} |
| long | ${tvShow.totalFilesize} |  | 
| List\<MediaTrailer\> | ${tvShow.trailer} |  | 
| int | ${tvShow.traktId} |  | 
| String | ${tvShow.tvdbId} | ${showTvdb} |
| MediaRating | ${tvShow.userRating} |  | 
| long | ${tvShow.videoFilesize} |  | 
| String | ${tvShow.videoHDRFormat} |  | 
| boolean | ${tvShow.videoIn3D} |  | 
| boolean | ${tvShow.watched} |  | 
| int | ${tvShow.year} | ${showYear} |

### TV show season ###

Entity name: **TvShowSeason**

| Type | Full access | Shortcut |
| --- | --- | --- |
| boolean | ${season.dummy} |  | 
| List\<TvShowEpisode\> | ${season.episodes} |  | 
| List\<TvShowEpisode\> | ${season.episodesForDisplay} |  | 
| Date | ${season.firstAired} |  | 
| Boolean | ${season.hasEpisodeImages} |  | 
| Boolean | ${season.hasEpisodeMetadata} |  | 
| List\<MediaFile\> | ${season.imagesToCache} |  | 
| Date | ${season.lastWatched} |  | 
| boolean | ${season.locked} |  | 
| List\<MediaFile\> | ${season.mediaFiles} |  | 
| boolean | ${season.newlyAdded} |  | 
| int | ${season.season} |  | 
| String | ${season.title} | ${seasonName} |
| long | ${season.totalFilesize} |  | 
| TvShow | ${season.tvShow} |  | 
| long | ${season.videoFilesize} |  | 
| boolean | ${season.watched} |  | 

### TV show episode ###

Entity name: **TvShowEpisode**

| Type | Full access | Shortcut |
| --- | --- | --- |
| List\<Person\> | ${episode.actors} |  | 
| int | ${episode.airedEpisode} |  | 
| int | ${episode.airedSeason} |  | 
| Map\<String,MediaFile\> | ${episode.artworkMap} |  | 
| Map\<String,String\> | ${episode.artworkUrls} |  | 
| String | ${episode.certification} |  | 
| String | ${episode.dataSource} |  | 
| Date | ${episode.dateAdded} |  | 
| String | ${episode.dateAddedAsString} |  | 
| Date | ${episode.dateAddedForUi} |  | 
| UUID | ${episode.dbId} |  | 
| String | ${episode.decadeLong} |  | 
| String | ${episode.decadeShort} |  | 
| List\<Person\> | ${episode.directors} |  | 
| String | ${episode.directorsAsString} |  | 
| boolean | ${episode.disc} |  | 
| int | ${episode.displayEpisode} |  | 
| int | ${episode.displaySeason} |  | 
| boolean | ${episode.dummy} |  | 
| boolean | ${episode.duplicate} |  | 
| int | ${episode.dvdEpisode} | ${episodeNrDvd} |
| boolean | ${episode.dvdOrder} |  | 
| int | ${episode.dvdSeason} |  | 
| int | ${episode.episode} | ${episodeNr} |
| Date | ${episode.firstAired} |  | 
| String | ${episode.firstAiredAsString} |  | 
| String | ${episode.firstAiredFormatted} |  | 
| boolean | ${episode.hasNote} |  | 
| boolean | ${episode.hasSubtitles} |  | 
| Map\<String,Object\> | ${episode.ids} |  | 
| List\<MediaFile\> | ${episode.imagesToCache} |  | 
| String | ${episode.imdbId} | ${episodeImdb} |
| String | ${episode.lastScrapeLanguage} |  | 
| String | ${episode.lastScraperId} |  | 
| Date | ${episode.lastWatched} |  | 
| boolean | ${episode.locked} |  | 
| MediaFile | ${episode.mainFile} |  | 
| MediaFile | ${episode.mainVideoFile} |  | 
| List\<MediaFile\> | ${episode.mediaFiles} |  | 
| List\<MediaFile\> | ${episode.mediaFilesContainingAudioStreams} |  | 
| List\<MediaFile\> | ${episode.mediaFilesContainingSubtitles} |  | 
| float | ${episode.mediaInfoAspectRatio} |  | 
| Float | ${episode.mediaInfoAspectRatio2} |  | 
| String | ${episode.mediaInfoAspectRatio2AsString} | ${aspectRatio2} |
| String | ${episode.mediaInfoAspectRatioAsString} | ${aspectRatio} |
| List\<String\> | ${episode.mediaInfoAudioChannelList} | ${audioChannelList} |
| String | ${episode.mediaInfoAudioChannels} | ${audioChannels} |
| String | ${episode.mediaInfoAudioCodec} | ${audioCodec} |
| String | ${episode.mediaInfoAudioCodecAndChannels} |  | 
| List\<String\> | ${episode.mediaInfoAudioCodecList} | ${audioCodecList} |
| String | ${episode.mediaInfoAudioLanguage} | ${audioLanguage} |
| List\<String\> | ${episode.mediaInfoAudioLanguageList} | ${audioLanguageList} |
| String | ${episode.mediaInfoContainerFormat} |  | 
| double | ${episode.mediaInfoFrameRate} |  | 
| String | ${episode.mediaInfoSource} |  | 
| List\<String\> | ${episode.mediaInfoSubtitleLanguageList} | ${subtitleLanguageList} |
| int | ${episode.mediaInfoVideoBitDepth} | ${videoBitDepth} |
| int | ${episode.mediaInfoVideoBitrate} |  | 
| String | ${episode.mediaInfoVideoCodec} | ${videoCodec} |
| String | ${episode.mediaInfoVideoFormat} | ${videoFormat} |
| String | ${episode.mediaInfoVideoResolution} | ${videoResolution} |
| String | ${episode.mediaSource} | ${mediaSource} |
| boolean | ${episode.multiEpisode} |  | 
| boolean | ${episode.newlyAdded} |  | 
| String | ${episode.note} | ${note} |
| String | ${episode.originalFilename} | ${originalFilename} |
| String | ${episode.originalTitle} | ${originalTitle} |
| String | ${episode.otherIds} |  | 
| String | ${episode.parent} |  | 
| String | ${episode.path} |  | 
| Path | ${episode.pathNIO} |  | 
| int | ${episode.playcount} |  | 
| String | ${episode.plot} |  | 
| String | ${episode.productionCompany} |  | 
| List\<String\> | ${episode.productionCompanyAsArray} |  | 
| MediaRating | ${episode.rating} |  | 
| Map\<String,MediaRating\> | ${episode.ratings} |  | 
| Date | ${episode.releaseDate} |  | 
| int | ${episode.runtime} |  | 
| int | ${episode.runtimeFromMediaFiles} |  | 
| int | ${episode.runtimeFromMediaFilesInMinutes} |  | 
| boolean | ${episode.scraped} |  | 
| int | ${episode.season} |  | 
| boolean | ${episode.stacked} |  | 
| List\<String\> | ${episode.tags} | ${episodeTags} |
| String | ${episode.tagsAsString} |  | 
| String | ${episode.title} | ${title} |
| String | ${episode.titleForUi} |  | 
| String | ${episode.titleSortable} | ${titleSortable} |
| String | ${episode.tmdbId} | ${episodeTmdb} |
| long | ${episode.totalFilesize} |  | 
| String | ${episode.traktTvId} |  | 
| TvShow | ${episode.tvShow} |  | 
| UUID | ${episode.tvShowDbId} |  | 
| TvShowSeason | ${episode.tvShowSeason} |  | 
| String | ${episode.tvdbId} | ${episodeTvdb} |
| boolean | ${episode.uncategorized} |  | 
| MediaRating | ${episode.userRating} |  | 
| String | ${episode.videoBasenameWithoutStacking} |  | 
| List\<MediaFile\> | ${episode.videoFiles} |  | 
| long | ${episode.videoFilesize} |  | 
| String | ${episode.videoHDR} | ${hdr} |
| String | ${episode.videoHDRFormat} | ${hdrformat} |
| boolean | ${episode.videoIn3D} |  | 
| boolean | ${episode.watched} |  | 
| List\<Person\> | ${episode.writers} |  | 
| String | ${episode.writersAsString} |  | 
| int | ${episode.year} | ${year} |

### Person ###

Entity name: **Person**

| Type | Full access | Shortcut |
| --- | --- | --- |
| Map\<String,Object\> | ids |  | 
| String | name |  | 
| String | nameForStorage |  | 
| String | profileUrl |  | 
| String | role |  | 
| String | thumbUrl |  | 
| String | type |  |

### Rating ###

Entity name: **MediaRating**

| Type | Full access | Shortcut |
| --- | --- | --- |
| String | id |  | 
| int | maxValue |  | 
| float | rating |  | 
| float | ratingNormalized |  | 
| int | votes |  | 

### Media file ###

Entity name: **MediaFile**

| Type | Full access | Shortcut |
| --- | --- | --- |
| boolean | DVDFile |  | 
| boolean | HDDVDFile |  | 
| boolean | HDR |  | 
| boolean | ISO |  | 
| boolean | animatedGraphic |  | 
| Float | aspectRatio |  | 
| Float | aspectRatio2 |  | 
| int | audioChannelCount |  | 
| String | audioChannels |  | 
| List\<String\> | audioChannelsList |  | 
| String | audioCodec |  | 
| List\<String\> | audioCodecList |  | 
| String | audioCodecListAsAstring |  | 
| String | audioLanguage |  | 
| List\<String\> | audioLanguagesList |  | 
| List\<MediaFileAudioStream\> | audioStreams |  | 
| List\<String\> | audioTitleList |  | 
| String | basename |  | 
| int | bitDepth |  | 
| String | bitDepthString |  | 
| boolean | blurayFile |  | 
| String | combinedCodecs |  | 
| String | containerFormat |  | 
| Date | dateCreated |  | 
| Date | dateLastModified |  | 
| MediaFileAudioStream | defaultOrBestAudioStream |  | 
| boolean | discFile |  | 
| int | duration |  | 
| String | durationHHMMSS |  | 
| String | durationHM |  | 
| String | durationHMS |  | 
| int | durationInMinutes |  | 
| String | durationShort |  | 
| String | exactVideoFormat |  | 
| String | extension |  | 
| Map\<String,String\> | extraData |  | 
| Path | file |  | 
| Path | fileAsPath |  | 
| long | filedate |  | 
| String | filename |  | 
| String | filenameWithoutStacking |  | 
| long | filesize |  | 
| String | filesizeInMegabytes |  | 
| double | frameRate |  | 
| boolean | graphic |  | 
| String | hdrFormat |  | 
| boolean | mainDiscIdentifierFile |  | 
| boolean | musicTheme |  | 
| int | overallBitRate |  | 
| String | overallBitRateInKbps |  | 
| boolean | packed |  | 
| String | path |  | 
| Float | pixelAspectRatio |  | 
| int | stacking |  | 
| String | stackingMarker |  | 
| List\<String\> | subtitleLanguagesList |  | 
| List\<MediaFileSubtitle\> | subtitles |  | 
| String | subtitlesAsString |  | 
| boolean | textual |  | 
| String | title |  | 
| String | type |  | 
| boolean | validMediainfoFormat |  | 
| boolean | video |  | 
| String | video3DFormat |  | 
| int | videoBitRate |  | 
| String | videoBitRateInKbps |  | 
| String | videoCodec |  | 
| String | videoDefinitionCategory |  | 
| boolean | videoDefinitionHD |  | 
| boolean | videoDefinitionLD |  | 
| boolean | videoDefinitionSD |  | 
| boolean | videoDefinitionUHD |  | 
| String | videoFormat |  | 
| int | videoHeight |  | 
| String | videoResolution |  | 
| int | videoWidth |  | 

### Audio stream ###

Entity name: **MediaFileAudioStream**

| Type | Full access | Shortcut |
| --- | --- | --- |
| int | audioChannels |  | 
| String | audioTitle |  | 
| int | bitDepth |  | 
| String | bitDepthAsString |  | 
| int | bitrate |  | 
| String | bitrateInKbps |  | 
| String | codec |  | 
| boolean | defaultStream |  | 
| boolean | forced |  | 
| String | language |  | 
| boolean | sdh |  | 
| String | title |  | 

### Subtitle ###

Entity name: **MediaFileSubtitle**

| Type | Full access | Shortcut |
| --- | --- | --- |
| String | codec |  | 
| boolean | defaultStream |  | 
| boolean | forced |  | 
| String | language |  | 
| boolean | sdh |  | 
| String | title |  | 

### Trailer ###

Entity name: **MediaTrailer**

| Type | Full access | Shortcut |
| --- | --- | --- |
| Date | date |  | 
| String | id |  | 
| Boolean | inNfo |  | 
| String | name |  | 
| String | provider |  | 
| String | quality |  | 
| String | scrapedBy |  | 
| String | url |  | 

### File type ###

Entity name: **MediaFileType**

| Type | Full access | Shortcut |
| --- | --- | --- |
| VIDEO | main video files |
| TRAILER | trailer |
| SAMPLE | sample != trailer |
| AUDIO | audio files |
| SUBTITLE | subtitle files |
| NFO | NFO files |
| POSTER | poster |
| FANART | fanart |
| BANNER | banner |
| CLEARART | clearart |
| DISCART | disc art |
| LOGO | logo |
| CLEARLOGO | clear logo |
| THUMB | thumb |
| CHARACTERART |character art |
| KEYART |key art |
| SEASON_POSTER | season poster |
| SEASON_BANNER | season banner |
| SEASON_THUMB | season thumb |
| SEASON_FANART | season fanart |
| EXTRAFANART | extra fanart |
| EXTRATHUMB | extra thumb |
| EXTRA | any extra file |
| GRAPHIC | generic graphic |
| MEDIAINFO | xxx-mediainfo.xml |
| VSMETA | xxx.ext.vsmeta Synology |
| THEME | "theme" files for some skins, like theme.mp3 (or bg video) |
| TEXT | various text infos, like BDinfo.txt or others... |
| UNKNOWN | |

### Source ###

Entity name: **MediaSource**

| Type | Full access | Shortcut |
| --- | --- | --- |
| String | name |  | 