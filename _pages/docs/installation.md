---
layout: single
permalink: "/docs/installation"
title: "Installation"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---
tinyMediaManager is a Java application which will run on Windows as well as macOS and Linux. Starting from v4.0 tinyMediaManager ships its own Java (except the ARM build) so there is no need to have Java installed on your system. Because there are little differences between these operating systems, tinyMediaManager’s installation behavior is a bit different.

On every system tinyMediaManager runs an automated update check at startup. This ensures the installation will stay up to date (with the latest bug fixes and features).

### Windows
tinyMediaManager can either be downloaded as an installable version or a portable version.

Installable:

Just download the installer package fro our webserver and install tinyMediaManager to the desired location. You **should not** install tinyMediaManager as Administrator because this tend to produce more problem than it solves.

Portable: 

tinyMediaManager is designed to be a portable application: this means that you can simply extract it to your preferred location (e.g. the users directory, or any other hard drive/network share). There is no need to install tinyMediaManager into C:\\Program Files\\.

### Mac OSX
In Mac OSX the application ships as an .app. All files (program files as well as database/cache and logs) are being stored inside the app.

Simply drag and drop the .app from the zip file to your applications folder and start tinyMediaManager from there.

**BE AWARE**: you cannot start tinyMediaManager from within the compressed file (.zip)! Executing from the downloads folder is not possible due to security mechanics of macOS.

### Linux
In Linux tinyMediaManager comes as a packaged file. You should extract all the files from the archive into a folder of your choice and then either use the tinyMediaManager binary or create a Desktop Shortcut by executing the following script on your terminal **inside from your tinyMediaManager's instance folder**.

```shell
cat <<EOM >~/.local/share/applications/tinyMediaManager.desktop
[Desktop Entry]
Type=Application
Terminal=false
Name=tinyMediaManager
Icon=$PWD/tmm.png
Exec=$PWD/tinyMediaManager
EOM
```

**When tinyMediaManager claims that libmediainfo is not working, just install an official version from [https://mediaarea.net/en/MediaInfo/Download](https://mediaarea.net/en/MediaInfo/Download)!**

Note: In case you get a **blank window** when starting tinyMediaManager, <a href="#blank-window">see below</a> for a possible solution.

#### Raspberry Pi (or other ARM devices)
tinyMediaManager does not ship a packaged Java for ARM devices. If you want to run tinyMediaManager on such a device, you have either to download the appropriate Java runtime (JRE) from [AdoptOpenJDK](https://adoptopenjdk.net/releases.html) (Java 11 or higher) and put the extracted runtime into a subfolder called `jre` of your tinyMediaManager installation (the executable must be in `jre/bin/java` to be picked up by the starter).

### Data/Settings/Cache/Backup locations
tinyMediaManager is designed to encapsulate all generated data as settings, databases, cache, logs and backups into its own folder. But there are some setups which required to have these data in another folder. The following system environment variables help to get these data written to another folders:

```properties
-Dtmm.contentfolder=
```

set the parent folder of all these _data_ folders. e.g. `-Dtmm.contentfolder=/home/user/.tmm`

```properties
-Dtmm.datafolder=
-Dtmm.cachefolder=
-Dtmm.logfolder=
-Dtmm.backupfolder=
```

can set all folders differently. e.g. `-Dtmm.datafolder=/home/user/.tmm/data`, `-Dtmm.logfolder=/var/log/tmm`.

These parameters can either be set via an file called `extra.txt` inside the tinyMediaManager folder (statically) or via the starting shellscript (dynamically - with variable expansion like `-Dtmm.contentfolder=${HOME}/.tmm`).
