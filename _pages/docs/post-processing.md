---
layout: single
permalink: "/docs/post-processing"
title: "Post Processing"

sidebar:
  nav: "docs"
---

Using our post processing feature you are able to trigger external tools/commands on your media files from within tinyMediaManager. This will allow you to execute scripts/features which are not available in tinyMediaManager but doable with external tools. The access to movie related data which is passed to the external tools can be done with [JMTE](/docs/jmte/).

## Settings

First of all you need to configure your commands in the settings. For example if you want to include a new post processing task for movies, you need to do to "Settings -> Movies -> Post-Processing":

<a class="fancybox" href="{{ site.urlimg }}2021/08/movie-post-process-settings.png" rel="post" title="Movie Post-Process settings">![Movie Post-Process settings]({{ site.urlimg }}2021/08/movie-post-process-settings.png "Movie Post-Process settings"){: .align-center}</a>

<a class="fancybox" href="{{ site.urlimg }}2021/08/movie-post-process-dialog.png" rel="post" title="Add/Edit Post-Process action">![Add/Edit Post-Process action]({{ site.urlimg }}2021/08/movie-post-process-dialog.png "Add/Edit Post-Process action"){: .align-center}</a>

### Basics 

Post processing can be used in 2 different modes:

* file mode: start an executable, and use the arguments as parameters
* scripting mode: no executable, just inline scripting in argument box

Some noteworthy facts:  
\- the commands are always execute in the folder of the main video/episode file  
\- always quote your file paths to get them right with spaces inside!  
\- prefer single quotes '' before double ones "".  
\- it makes a difference, if you add all parameters into a single line, or use multiple lines (depends on program).
{: .notice--primary}

### Windows
file mode exe/com/ps1
- executed via `powershell`
- works on network shares
- every line in arguments is ONE parameter (even with space!)

file mode bat/cmd
- executed via `cmd /C`
- DOES NOT work on network shares! (you need to have your data source mapped to a drive letter!)
- arguments are joined into single string, every parameter available via %1 %2 ...

scripting mode
- executed via `powershell -Command`
- full powershell syntax!
- commands MUST be delimited via ";" (either on single or multi line)

### macOS / Linux
file mode and scripting mode
- executed via `/bin/sh -c`
- full shell syntax!

### Examples:
Use inline scripting; print directory & title, and add plot to some text file.
Note the delimiter!
```json
    "name" : "plot",
    "path" : "",
    "command" : "dir;\necho '${movie.title}';\necho '${movie.plot}' >plot.txt;"
```

Execute PowerShell PS1 scripting file.
Note the parameters - every line is ONE parameter (and can contain spaces)
Useful for movie path or similar.
```json
    "name" : "ps1 space",
    "path" : "C:\\do something.ps1",
    "command" : "param1\nparam2_1 param2_2\nparam3\nparam with space"
```

MediaInfoGUI
```json
    "name" : "MediaInfo",
    "path" : "C:\\Program Files\\MediaInfo\\MediaInfo.exe",
    "command" : "'${movie.mainVideoFile.path}/${movie.mainVideoFile.filename}'"
```

## Usage 

After you have set up your post process tasks, you can use them in the right click menu of the movie list: 

<a class="fancybox" href="{{ site.urlimg }}2021/08/movie-post-process.png" rel="post" title="Movie Post-Process actions">![Movie Post-Process actions]({{ site.urlimg }}2021/08/movie-post-process.png "Movie Post-Process actions"){: .align-center}</a>

The chosen post process action will be applied to every selected movie.

This feature is also available for TV shows and episodes. You will find the corresponding settings under "Settings -> TV shows -> Post-Processing". The context menu for TV show processing is built the same way as the one for movies.
{: .notice--primary}