---
layout: single
permalink: "/docs/upgrade-v4"
title: "Manual upgrade to v4"

sidebar:
  nav: "docs"
---
There are two different ways to upgrade an existing tinyMediaManager v3 installation to v4.

## Clean v4 installation
The first way (preferred at the moment) would be to download a fresh copy of tinyMediaManager from [https://release.tinymediamanager.org](https://release.tinymediamanager.org) and copy over your data from the v3 installation to the new v4 installation.

1. Download the most recent build of tinyMediaManager v4 from [https://release.tinymediamanager.org](https://release.tinymediamanager.org).
2. Extract the archive to your preferred destination.
3. Copy the folders `data` and `cache` from your v3 installation over to the v4 installation. macOS users must right-click on the tinyMediaManager.app, choose `Show package contents` and navigate to `Contents->Resources->Java` to find those folders.
4. Start tinyMediaManager v4 and you should have all data and settings from v3 (except some settings which have changed between v3 and v4).

Using this way you will get a _clean_ new v4 installation and also have the v3 installation as some sort of backup.

## Upgrade the existing v3 installation
You can upgrade your existing v3 installation with redirecting the updater to v4:

1. Go to your tinyMediaManager v3 folder. macOS users must right-click on the tinyMediaManager.app, choose `Show package contents` and navigate to `Contents->Resources->Java` to find the folders.
2. Open the file `getdown.txt` with any text editor
3. Change the `appbase` url from `https://release.tinymediamanager.org/v3/build/` to `https://release.tinymediamanager.org/v4/build/` (just **v3** has changed to **v4**)
4. Start tinyMediaManager (or directly the updater) to trigger the new update.
5. tinyMediaManager will download the v4 upgrade and must restart once more to fully apply it.

Using this way you will upgrade your existing v3 installation to the v4 installation. After the upgrade finished, you will have a backup (`v3-backup.zip`) of your old data in the `backup` folder.
