---
layout: single
permalink: "/help/faq-macos"
title: "Frequently Asked Questions - macOS"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "help"
---

## tinyMediaManager won't start

* make sure you copied tinyMediaManager to your applications folder after downloading. For security reasons macOS executes apps from the downloads folder in a read only sandbox but tinyMediaManager needs write access (to store settings, database, logs, ...)
* make sure you did not extract the tinyMediaManager app directly to your applications folder. The tinyMediaManager app should be extracted in your downloads folder before moving to the applications folder
* make sure Java is installed (open the terminal/command line via Spotlight -> terminal) and type
  `java -version` this should print the installed Java version of your system (1.6, 1.7 or 1.8). If the program is not found, please install the latest Java version
* if Java is installed, but tinyMediaManager won't start, there could be a Java library in your /Library/Java/Extensions which interfere with tinyMediaManager. In this case try to create a file called `extra.txt` in your tinyMediaManager folder (right click the tinyMediaManager.app -> Show package contents -> navigate to Contents - Resources - Java) with the following content: `-Djava.ext.dirs=`
* if tinyMediaManager still won't start, please have a look at the FAQ for reporting bugs

**v4** ships its own Java, so all Java related points from above only apply to v3
{: .notice--primary}

## Does tinyMediaManager run on a mac using the Apple M1 chip

tinyMediaManager (v4) runs on devices with the new Apple M1 chip using the latest version of Rosetta 2.

## macOS complains that tinyMediaManager is from an unidentified developer

Due to the complex restrictions of Apple for App-Developers it is not possible to sign and notarize tinyMediaManager because that would break the whole App setup of tinyMediaManager (no more auto updates, no more "portable" mode, ...). Since tinyMediaManager is cross OS (Windows, Linux and macOS) it is built to work on all OS in the same way which results in a (proven) structure where we are not able to provide a signed/notarized App on macOS.

After you have downloaded tinyMediaManager, you need to follow the [official guide](https://support.apple.com/guide/mac-help/open-a-mac-app-from-an-unidentified-developer-mh40616/mac) to open it.

## Popup Dialogs in Big Sur lock up tinyMediaManager

macOS Big Sur recently changed the behavior of handling programs and their dialogs when in full screen mode. There is a problem with that behavior in all recent Java versions (see the [Java bug report](https://bugs.openjdk.java.net/browse/JDK-8256465?jql=labels%20%3D%20macos)). 

There is a workaround for that problem by deactivating this behavior for Java based programs:

1. Open a terminal
2. Enter the command `defaults write net.java.openjdk.cmd AppleWindowTabbingMode manual`
3. Restart tinyMediaManager

## Mounted Drives are not accessible after upgrading to Big Sur

macOS Big Sur did some changed to security policies and now tinyMediaManager isn't allowed to access several files. To allow tinyMediaManager to access your files do the following steps:

1. Go to System Preferences -> Security & Privacy -> Privacy and select „Files and Folders“
2. Add tinyMediaManager to the list of allowed apps