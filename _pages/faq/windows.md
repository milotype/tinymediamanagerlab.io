---
layout: single
permalink: "/help/faq-windows"
title: "Frequently Asked Questions - Windows"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "help"
---

## tinyMediaManager does not start from within the "Program Files" folder

First of all: tinyMediaManager is designed to be a portable application. This means that you can simply extract it to your preferred location (e.g. the users directory, or any other hard drive/network share). There is no need to install tinyMediaManager into C:\\Program Files\\.

If you still want to install tinyMediaManager to C:\\Program Files\\, you have to adopt permissions for this folder to run tinyMediaManager without Administrator (**you should never run tinyMediaManager as Administrator - there is absolutely no need to do that**):

Right click on the folder where you have installed tinyMediaManager (e.g. C:\\Program Files\\tinyMediaManager) and choose preferences. Go to the tab _Security_. In the list of all available users, choose `Users (machine name\Users)`. In the area below the users list, activate all permissions.

## tinyMediaManager.exe is reported to be a virus

The launcher from tinyMediaManager (`tinyMediaManager.exe`) is reported from several anti virus programs to be a virus. The launcher it self is written in `Go` and there is a FAQ from `Go` about this problem: [https://golang.org/doc/faq#virus](https://golang.org/doc/faq#virus):

> This is a common occurrence, especially on Windows machines, and is almost always a false positive. Commercial virus scanning programs are often confused by the structure of Go binaries, which they don't see as often as those compiled from other languages. 

> In any case, if you believe the report is in error, please report a bug to the supplier of your virus scanner. Maybe in time virus scanners can learn to understand Go programs. 

## tinyMediaManager can't connect to the internet via NTLM proxy
NTLM authentication does not work within tinyMediaManager. You can use something like [CNTLM](http://cntlm.sourceforge.net/) to connect to the internet.

## The user interface of tinyMediaManager is corrupted
Try to create (or edit if it already exists) a file called `launcher-extra.yml` in the tinyMediaManager install directory and add the following line under the node `jvmOpts:`, so it looks like

```yaml
jvmOpts:
- "-Dsun.java2d.nodraw=true"
```

and restart tinyMediaManager

**v3**: Try to create (or edit if it already exists) a file called `extra.txt` in the tinyMediaManager install directory and add the following line `-Dsun.java2d.nodraw=true` and restart tinyMediaManager
{: .notice--warning}

## tinyMediaManager won't start with Java Portable
Java Portable provides Java in a different way than the installed one. You may need to create a batch file (.bat) inside the tinyMediaManager folder with the following content (including the dot at the end!):
```powershell
cd %~dp0
<path to PortableApps>\CommonFiles\Java\bin\javaw.exe -Djava.net.preferIPv4Stack=true -Dsilent=noupdate -Dfile.encoding=UTF-8 -jar getdown.jar .
```

replace `<path to PortableApps>` with the installation path of your PortableApps.

tinyMediaManager v4 ships its own Java, so this is not needed any more
{: .notice--primary}

### tinyMediaManager does not respect UI scaling settings from the system
Every Java version/Operating System does handle high DPI/UI scaling differently, but you can force tinyMediaManager to pick up your preferred scaling factor:

1. open (or create if it does not exist) the file `launcher-extra.yml` in the tinyMediaManager folder
2. add (or modify) the following part in this file:
```yaml
jvmOpts:
- "-Dsun.java2d.uiScale=2"
```
where `2` is the scaling factor you want to use.
3. relaunch tinyMediaManager

**v3**: *Java 8*: follow the steps from [this thread](https://superuser.com/questions/988379/how-do-i-run-java-apps-upscaled-on-a-high-dpi-display/1207925#1207925).  
*Java 9+*: either do the same as for *Java 8* or create a file called `extra.txt` in the tinyMediaManager install directory and add the following line `-Dsun.java2d.uiScale=2`. Last but not least you could try to increase the font size in the general settings and restart tinyMediaManager
{: .notice--warning}

### UI glitches
tinyMediaManager on Windows (locally or with a remote desktop connection software like RDP or VNC) can produce some weird glitches due an unresolved issue of Java (in combination with some graphics drivers). You can turn off the direct3D rendering pipeline to avoid these glitches:

1. open (or create if it does not exist) the file `launcher-extra.yml` in the tinyMediaManager folder
2. add (or modify) the following part in this file:
```yaml
jvmOpts:
- "-Dsun.java2d.d3d=false"
```
3. relaunch tinyMediaManager

**v3**: You can create (or edit if it already exists) a file called `extra.txt` in the tinyMediaManager install directory and add the following line `-Dsun.java2d.d3d=false`
{: .notice--warning}

### Getting many TLS/SSL errors - PKIX certificate chain validation failed

This usually happens, when a SSL certificate of a contacted site has expired.  
But another prominent reason could be some corporate/enterprise SSL inspection, where the certificate chain gets broken up, and re-signed by some proxy server. This is common for companies, which roll out their own root certificate to your machine.

Since Java does not read that per default, you could change that behaviour within `launcher-extra.yml`

```yaml
jvmOpts:
- "-Djavax.net.ssl.trustStoreType=WINDOWS-ROOT"
```

Setting that delegates Java to use the Windows internal certificate store, which might have the missing one to validate your requests.

