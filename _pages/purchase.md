---
title: "Purchase a tinyMediaManager license"

layout: splash
permalink: "/purchase/"

sidebar:
  nav: "purchase"

paddle: true
---
<script type="text/javascript">
	Paddle.Setup({ 
    vendor: 111552,
    completeDetails: true
  });
</script>

<h1>Purchase a license</h1>

You can [download tinyMediaManager](/download/) and use it for free in a limited way (**FREE** version). If you want to unlock all features you need to buy a license for the **PRO** version. They key differences are:

- **FREE** version:
  + base functionality of tinyMediaManager (update data sources, scrape, write/read NFO files, rename, edit, export, command line interface, ...)
  + TMDB scraper

- **PRO** version:
  + all other scrapers (IMDB, OMDB, Universal, Kodi, ...)
  + trailer download
  + subtitle download
  + Trakt.tv integration
  + FFmpeg integration

For a detailed comparison of the versions, please have a look at the [version fact sheet](/docs/versions/).
{: .notice--warning}

If you want to unlock all features of tinyMediaManager (**PRO** version) you need to buy a 1-year license via [Paddle.com](https://www.paddle.com) for **10€** (tax included). The license is per user, so you can install tinyMediaManager on as many machines as you wish.

If you have contributed to tinyMediaManager is some way (translations, bug hunting, forums activity, ...) and feel eligible to receive a free contributor license, please [contact us](/help/). 

<div id="purchase">
  <a id="paddle" href="#" class="paddle_button" data-allow-quantity="false" data-referrer="purchase" data-product="589766" data-init="true">Purchase <em><b>1 year</b></em> license <br/><small>10€ (tax included)</small></a>
</div>
<br />

## Please take care of the following things before buying a license

- You agree with our [End User License Agreement](/eula/)
- Our order process is conducted by our online reseller [Paddle.com](https://www.paddle.com). [Paddle.com](https://www.paddle.com) is the Merchant of Record for all our orders. Paddle provides all customer service inquiries and handles returns. If you encounter any problems while purchasing a license please [contact Paddle.com](https://paddle.net/contact) for further help.
- Please type your email address correctly when purchasing a license. The license code will be sent to this email address after the purchase is finished.
- If you did not receive an email from [Paddle.com](https://www.paddle.com) please check your *Spam / Junk Email folder*. If you have not received any email at all then you have likely mistyped your email address or your mail server is rejecting emails. In this case, please contact [Paddle.com](https://paddle.net/contact) with your *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.
- A license is valid on all platforms and can be used on multiple machines.
- After you have received your license code, you can unlock tinyMediaManager directly from within the application. If you use the command line version, just paste the license code **without any extra spaces/newlines** into a file called `/data/tmm.lic` inside your tinyMediaManager folder.
- If you have problems activating your license, please have a look at our [issue tracker](https://gitlab.com/tinyMediaManager/tinyMediaManager/issues) if there is already a solution for your problem or contact us directly at [GitLab](https://gitlab.com/tinyMediaManager/tinyMediaManager/issues/new?issuable_template=Bug).
- If you have lost your license code, please [contact Paddle.com](https://paddle.net/contact) with your *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.
- You may apply for a refund of the License fee within 30 days of receipt by [contacting Paddle.com](https://paddle.net/contact)
