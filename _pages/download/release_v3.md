---
title: "Download"
layout: single
classes: wide

permalink: /download/release-v3/

sidebar:
  nav: "downloads"
---

**tinyMediaManager v3 has reached end of life (EOL).**

You can find old downloads in our [archive](https://archive.tinymediamanager.org).