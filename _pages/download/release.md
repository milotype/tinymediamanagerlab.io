---
title: "Download"
layout: single
classes: wide

permalink: /download/

sidebar:
  nav: "downloads"
---
You can download and evaluate tinyMediaManager for free (with some limitations. [More details >>](/purchase/)).

<div class="iframe-container">
  <iframe src="https://release.tinymediamanager.org/download_v4.html" width="100%" frameborder="0" >
    <p>You find the latest release build <a href="https://release.tinymediamanager.org">https://release.tinymediamanager.org</a></p>
  </iframe>
</div>

<br />
All downloads for the current release are available at [https://release.tinymediamanager.org](https://release.tinymediamanager.org).

<br />
Old builds are available in our [release archive](https://archive.tinymediamanager.org).