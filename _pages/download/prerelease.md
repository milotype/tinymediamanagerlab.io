---
title: Pre-Release download
layout: single
classes: wide

permalink: /download/prerelease/

sidebar:
  nav: "downloads"
---

Pre-Release builds are built a few days before we publish a new release. This build is rather stable but still can contain some bugs. This build should be used for testing purposes only.

You can download and evaluate tinyMediaManager for free (with some limitations. [More details >>](/purchase/)).

<div class="iframe-container">
  <iframe src="https://prerelease.tinymediamanager.org/download_v4.html" width="100%" frameborder="0" >
    <p>You find the latest pre-release build at <a href="https://prerelease.tinymediamanager.org">https://prerelease.tinymediamanager.org</a></p>
  </iframe>
</div>  

<br />
All downloads are available at [https://prerelease.tinymediamanager.org](https://prerelease.tinymediamanager.org).