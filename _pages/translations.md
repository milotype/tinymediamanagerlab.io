---
title:  "Translations"

layout: single
permalink: "/docs/translations"

sidebar:
  nav: "docs"
---
tinyMediaManager is translated via [Weblate](https://hosted.weblate.org/engage/tinymediamanager/) by the community:

[![Translation status](https://hosted.weblate.org/widgets/tinymediamanager/-/horizontal-auto.svg)](https://hosted.weblate.org/engage/tinymediamanager/)

If you want to improve the translation for your language or add a new language, just head over to [Weblate](https://hosted.weblate.org/engage/tinymediamanager/) and start translating!
