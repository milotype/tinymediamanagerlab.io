---
title: Regular expression based skip folders
permalink: /blog/regular-expression-based-skip-folders/
categories:
  - News
tags:
  - v4
  - v4.2
summary:
  - image: tmm.png
---

In addition to the _absolute path_ based skip folders, tinyMediaManager v4.2 adds the ability to add skip folders for movies which are based on _regular expressions_. With regular expressions you have a better utility to exclude folders from the _update data source_ scan as with the path based approach.

To use a regular expression in your skip folder, you just need to press the button below the one for adding a full path in the skip folder section:

<a class="fancybox" href="{{ site.urlimg }}2021/07/skip-folders.png" rel="post" title="Add a regular expression based skip folder">![Add a regular expression based skip folder]({{ site.urlimg }}2021//07/skip-folders.png "Add a regular expression based skip folder"){: .align-center}</a>

And then add the regular expression you like to use for your skip folder:

<a class="fancybox" href="{{ site.urlimg }}2021/07/regular-expression.png" rel="post" title="Enter a regular expression">![Enter a regular expression]({{ site.urlimg }}2021/07/regular-expression.png "Enter a regular expression"){: .align-center}</a>

To keep the handling of the regular expressions easy, only the actual folder name will be checked against the regular expression (and not the full path).