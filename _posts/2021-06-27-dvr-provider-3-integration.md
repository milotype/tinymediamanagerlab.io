---
title: DVR-Provider 3 integration
permalink: /blog/dvr-provider-3-integration/
categories:
  - News
tags:
  - v4
  - v4.2
summary:
  - image: tmm.png
---

NFO files created with tinyMediaManager got a first level support in the tool [DVR-Provider 3](https://dvr-provider-3.haenlein-software.com/) by Haenlein-Software.

## DVR-Provider 3

DVR-Provider 3 is a video management tool for your private movie archive. From private movies, TV recordings to copies of DVDs and BluRays as movie files or ISO images, it manages all common formats (TS, M2TS, MPEG, MP4, MKV, AVI) in SD, HD and UHD. Movies are sent lossless and in full quality over the network from DVR Provider 3 to the registered playback devices (receivers, TV devices, DVD and BluRay players). 
 
The DVR-Provider 3 is installed on a Windows PC and consists of 2 program parts: 

### The admin interface

In the admin interface you can manage your movies, devices and users. There you can import and manage your movies - the storage locations for your movies can be local and USB hard drives, network drives (NAS) or even cloud drives. There are 2 possible ways to import meta data for your movies:

* If the internet search was switched off, the DVR-Provider 3 searches in the movie directory for local tinyMediaManager data and copies them into DVR-Provider 3. If your movies are managed by tinyMediaManager, there is no need for post-processing in DVR-Provider 3 at all. 
* Alternatively DVR-Provider 3 can also import information directly from the internet - if the Internet search is switched on. 

DVR-Provider 3 stored its data into an own data base. You can access this data via an integrated web site in the admin interface from your local network. 

Playback devices registered in DVR-Provider 3 can be assigned to registered family members. Each family member gets its own access to the movie archive. Parents can thus share movies with their children depending on their age by using an integrated parental control. 

### The user interface:

You can use any device with a browser (smartphones, tablets, laptops, PC and possibly also TV or receiver) as a client for DVR-Provider 3 - only the local website of DVR-Provider 3 is displayed. Via the user interface, you can select a movie from you database. Besides movie description, cover, movie trailer, actor list with pictures and their websites, all technical information about a movie is also available. The user interface offers not only a simple text search - also a [complex search](http://dvr-provider-3.haenlein-software.com/hilfe/index.php?title=WebServer:_Suchen) system is available.

All data, as well as playback position in the movie, personal ratings and date of use constantly flow into the database. With this data you can manage and organize the archive extensively on the admin interface.

At [the homepage of DVR-Provider 3](http://DVR-Provider-3.Haenlein-Software.com) you will find a course of 18 videos under Support Videos, which show you all functions from import to operation. Thi user manual is an interactive media wiki. Manual and program (admin and user interface) are available in over 100 languages. You define the language in which the movie information is available either in tinyMediaManager or in DVR-Provider 3. 

## tinyMediaManager integration

There is also a [help page](http://dvr-provider-3.haenlein-software.com/hilfe/index.php?title=Import_vom_TinyMediaManager_(TMM)) how to integrate NFO files created by tinyMediaManager into [DVR-Provider 3](https://dvr-provider-3.haenlein-software.com/).

## Special offer

Users of tinyMediaManager can get a special discount for [DVR-Provider 3](https://dvr-provider-3.haenlein-software.com/) by using the coupon code **TinyMM** in their web shop.
{: .notice--warning}