---
title: 'Beta - r256'
permalink: /blog/beta-r256/
categories:
  - News
  - Release
tags:
  - v2    
summary:
  - image: release.png    
---
  * fixed creation of extrathumbs
  * added the option to get the tmdbid for moviesets in movieset editor window
<!--more-->
