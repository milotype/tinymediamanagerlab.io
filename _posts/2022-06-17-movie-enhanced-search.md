---
title: Enhanced Search in the Movie Module
permalink: /blog/movie-enhanced-search/
categories:
  - News
tags:
  - v4
  - v4.3
summary:
  - image: tmm.png
---

tinyMediaManager v4.3 offers a new enhancement for the search field in the movie module: similar to JMTE you can access to several different properties of your movie to filter for that value.

The syntax for the search is:

```
<search field>:<search value>
```

E.g. if you want to filter for movies with "James Cameron" as director, you can use

```
directors:james cameron
```

To filter for some word in the plot you can use

```
plot:manhattan
```

You find a list of available properties in our [JMTE docs](/docs/jmte/#available-data)