---
title: Version 2.9.16
permalink: /blog/version-2-9-16/
categories:
  - News
  - Release
tags:
  - v2
summary:
  - image: release.png
---
x MediaInfo reworked their naming schema for audio codecs - please reload your media information where needed!  
x TMDB-scraper: do not accept empty IDs as perfect match  
x fixed movie renamer to compare paths case sensitive  
x fixed an issue of wrong file date calculation (MacOS)  
x fixed truncated text on the movie settings screen #443  
