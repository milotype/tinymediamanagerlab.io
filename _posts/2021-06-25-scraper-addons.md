---
title: Third party scraper addons
permalink: /blog/third-party-scraper-addons/
categories:
  - News
tags:
  - v4
  - v4.2
summary:
  - image: tmm.png
---

tinyMediaManager's next release (v4.2) will introduce an interface for third party scrapers. Using this interface any developer can create a scraper and publish it. The user just needs to download the generated _.jar_ file and put it into the subfolder `addons` of the tinyMediaManager installation. After the next restart, tinyMediaManager will be able to use this scraper the same way as internal scrapers.

To create a custom scraper you can use (fork or clone) the template repository [https://gitlab.com/tinyMediaManager/scraperaddonsample](https://gitlab.com/tinyMediaManager/scraperaddonsample) which already offers the right structure for a custom scraper. This repository has one special class and some other examples.

## Scraper loader
* `SampleAddonProvider.java` - this class is the main connector between the custom scraper and tinyMediaManager itself. The Method `getAddonClasses` will offer tinyMediaManager a list of classes which offer scraper functions. The scraper classes itself need to implement the corresponding interfaces to tell tinyMediaManager which scraper functionalities are provided.

To enable the Java Service Provider (SPI) you also need to create a file called `META-INF/services/org.tinymediamanager.scraper.spi.IAddonProvider` containing the path to the loader class. In our example this file just needs one entry: `org.tinymediamanager.scraper.spisample.SampleAddonProvider`.

## Interfaces to be implemented
The third party scraper just needs to implement the interfaces where data can be provided. E.g. if the scraper just provides meta data for TV shows only the interface `ITvShowMetadataProvider` needs to be implemented.

Available interfaces:

* `IMovieMetadataProvider` - the class offers scraping of meta data for movies (example: `SampleMovieMetadataProvider.java`)
* `IMovieArtworkProvider` - the class offers scraping of artwork for movies (example: `SampleMovieArtworkProvider.java`)
* `IMovieTrailerProvider` - the class offers scraping of trailers for movies (example: `SampleMovieTrailerProvider.java`)
* `IMovieSubtitleProvider` - the class offers scraping of subtitles for movies (example: `SampleMovieSubtitleProvider.java`)

* `ITvShowMetadataProvider` - the class offers scraping of meta data for TV shows (example: `SampleTvShowMetadataProvider.java`)
* `ITvShowArtworkProvider` - the class offers scraping of artwork for TV shows (example: `SampleTvShowArtworkProvider.java`)
* `ITvShowTrailerProvider` - the class offers scraping of trailers for TV shows (example: `SampleTvShowTrailerProvider.java`)
* `ITvShowSubtitleProvider` - the class offers scraping of subtitles for TV shows (example: `SampleTvShowSubtitleProvider.java`)

## MediaProviderInfo
If you create an own scraper, you need some meta data for the scraper in addition to the pure code. This meta data is displayed in tinyMediaManager itself (scraper selection, settings, ...) and consist of:

* Scraper Id (do not re-use this id across different scrapers!)
* Scraper Sub-Id (especially interesting if you want to differentiate settings in the areas of the scraper. E.g. the movie meta data area needs other settings than the movie artwork area)
* Scraper Name
* Description (will be displayed in the settings)
* Logo (will be described in the settings and scraper drop downs)
* Configuration parameters (for fine tuning your scraper - e.g. custom API key, language selection, ...)

The meta data will be included in the `MediaProviderInfo` of the scraper:

```java
@Override
protected MediaProviderInfo createMediaProviderInfo() {
    MediaProviderInfo providerInfo = new MediaProviderInfo("spi-sample", "movie", "SPI Sample", "A sample for a dynamic movie metadata scraper", SampleMovieMetadataProvider.class.getResource("/org/tinymediamanager/scraper/spisample/tmm_logo.svg"));

    // create configuration properties
    providerInfo.getConfig().addText("text", "", false);
    providerInfo.getConfig().addBoolean("boolean", true);
    providerInfo.getConfig().addInteger("integer", 10);
    providerInfo.getConfig().addSelect("select", new String[]{"A", "B", "C"}, "A");

    // load any existing values from the storage
    providerInfo.getConfig().load();

    return providerInfo;
}
```  

## Available libs/tools
A creator of a third party library can use all libraries which tinyMediaManager uses itself. For a detailed list of used libraries please refer to the [pom.xml](https://gitlab.com/tinyMediaManager/tinyMediaManager/-/blob/devel/pom.xml) of the tinyMediaManager project. When there are extra libraries needed, you need to package them into the jar file using the [maven shade plugin](https://maven.apache.org/plugins/maven-shade-plugin/).