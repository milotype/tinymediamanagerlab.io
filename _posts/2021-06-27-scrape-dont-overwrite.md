---
title: Scrape - Do not overwrite options
permalink: /blog/scrape-dont-overwrite/
categories:
  - News
tags:
  - v4
  - v4.2
summary:
  - image: tmm.png
---

tinyMediaManager v4.2 introduces a feature where you can enable a "do not overwrite existing data" option when scraping. This becomes handy if you have manually entered data or added custom artwork for your movies/TV shows.

You can enable this protection in the search dialogs or scrape dialogs:

<a class="fancybox" href="{{ site.urlimg }}2021/06/overwrite-movie-chooser.png" rel="post" title="Do not overwrite in movie search">![Do not overwrite in movie search]({{ site.urlimg }}2021/06/overwrite-movie-chooser.png "Do not overwrite in movie search"){: .align-center}</a>

<a class="fancybox" href="{{ site.urlimg }}2021/06/overwrite-movie-scrape.png" rel="post" title="Do not overwrite in movie scrape">![Do not overwrite in movie scrape]({{ site.urlimg }}2021/06/overwrite-movie-scrape.png "Do not overwrite in movie scrape"){: .align-center}</a>

When this option is activated, tinyMediaManager does not overwrite existing data/images/trailers.