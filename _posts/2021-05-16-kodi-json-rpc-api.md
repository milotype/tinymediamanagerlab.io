---
title: Kodi JSON-RPC API
permalink: /blog/kodi-json-rpc-api/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---
tinyMediaManager included a basic implementation of the [Kodi JSON-RPC API](https://kodi.wiki/view/JSON-RPC_API) for a long time, but with release 4.1.5 this API has been reworked in several areas.

Using the [Kodi JSON-RPC API](https://kodi.wiki/view/JSON-RPC_API) you are able to connect your tinyMediaManager instance to your Kodi instance and remotely control some parts of Kodi. To make this work, you need to set up Kodi to [accept incoming connections](https://kodi.wiki/view/Settings/Services/Control) by enabling _Allow remote control via HTTP_:

<a class="fancybox" href="{{ site.urlimg }}2021/05/kodi-http.jpg" rel="post" title="Allow remote control via HTTP">![Allow remote control via HTTP]({{ site.urlimg }}2021/05/kodi-http.jpg "Allow remote control via HTTP"){: .align-center}</a>

After you have enabled the remote control in Kodi you can set up tinyMediaManager to connect to the Kodi instance. Inside tinyMediaManager go to "Settings -> General -> External devices" and enter the corresponding values of your Kodi setup. Afterwards you can test the connection to the Kodi instance.

At the moment the following functions are supported by tinyMediaManager:

- Remote control of Kodi
  - Mute
  - Set Volume
  - Quit
  - Hibernate/Reboot/Shutdown/Suspend System
- Manage data sources (Video/Audio)
  - Clean (remove non existent entries)
  - Scan (search for new items)
- Refresh movies/episodes metadata from NFO
- Receive events from Kodi

The main challenge for adding more functions is the "mapping" between entries in tinyMediaManager and Kodi (since movies or TV shows/episodes can be accessed in different ways, mapping between those paths can be complicated). With re-working the "Refresh movies/episodes metadata from NFO" function we added a better way to do this mapping which can be used for implementing other features in the future.