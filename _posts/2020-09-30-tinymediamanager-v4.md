---
title: Version v4
permalink: /blog/version-4-0/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
excerpt: After several months of development we are proud to announce tinyMediaManager v4...
---
After several months of development we are proud to announce

# tinyMediaManager v4

As we have already written in a [previous post](/blog/v4-future/) tinyMediaManager is changing from a free license model to a subscription based license model. The key arguments for this decision are that we can no more invest this amount of spare time into tinyMediaManager without any financial compensation. If you need more information about this decision, please read the corresponding [blog post](/blog/v4-future/).

## What does that mean?

The key facts about our new model are:

- tinyMediaManager remains Open Source - the code is still freely available (excluding our license module and the launcher)
- (v4.1+) tinyMediaManager comes in 2 flavors
  - FREE version:
    + base functionality of tinyMediaManager (update data sources, scrape, rename, edit, export, command line interface, ...)
    + TMDB scraper

  - PRO version:
    + all other scrapers (IMDB, OMDB, Universal, Kodi, ...)
    + trailer download
    + subtitle download
    + Trakt.tv integration
    + FFmpeg integration

- to use the PRO version, a license of tinyMediaManager can [be purchased](/purchase/) (10€ - tax included) and will be valid for one year (because of the ongoing maintenance and constant improvement of tinyMediaManager)
- No upgrade from v3 is needed! If you are satisfied with v3, you can still use it. But there will be no more features added in v3 - just **urgent bug fixes**.

Since we support the Open Source idea, we will also reward contributions from the community with free licenses (be it translations, pull/merge requests, donations, help in the forum, testing of test versions, ...). If you have contributed to tinyMediaManager is some way (translations, bug hunting, forums activity, ...) and feel eligible to receive a free contributor license, please [contact us](/help/). 

## What is new/different in v4?

Version 4.1
=======================
- individual scraper settings
- FFmpeg integration
- new scrapers (TVMaze, OMDB for TV shows)
- missing movies in movie sets
- enhanced Trakt.tv sync
- new rating implementation
- and many more changes under the hood...

Version 4.0
=======================

- bundled JRE and rewritten launcher (no installed Java needed any more)
- real HiDPI support (slightly changed look and feel to achieve correct UI scaling)
- mix in IMDB ratings for almost every scraper without the need of the universal scrapers
- universal TV show scraper
- completely reworked libmediainfo usage:
  - full support of reading/writing mediainfo XML files
  - better support of disc structures (.iso and VIDEO_TS/BDMV/HVDVD_TS folders)
  - several fixes for reading data out of libmediainfo
- enhanced filter management
- new command line interface
- mediainfo integration for Trakt.tv
- support for Windows taskbar / macOS dock access
- added a true regular expression search for movies/movie set/TV show titles
- display missing episodes in the season overview panel
- provide artwork from TMDB for Trakt.tv search
- added showlink support for movies
- better performance in regular expression searches
- fixed handling of MP2/MP3 streams
- enhanced downloading of youtube trailers
- KodiRPC: improve matching
- and many more changes under the hood...

## What happens with v3?
tinyMediaManager v3 will remain available and free, but only serious bugs will be fixed and no new features will be added. A mandatory upgrade to v4 will not be necessary, but we want to provide an easy transition for users which want to upgrade.

## Where can I get v4?

You can get tinyMediaManager v4 at [https://www.tinymediamanager.org/download/](https://www.tinymediamanager.org/download/).

## Can I upgrade my v3 installation to v4?

Yes you can upgrade your installation with some manual steps at the moment. Please visit the [docs](/docs/upgrade-v4/) to see how you can upgrade to v4.
