---
title: Thumb Generation with FFmpeg
permalink: /blog/thumb-generation-with-ffmpeg/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---

Starting from tinyMediaManager v4.1 you will be able to generate thumbs/stills from your video file using [FFmpeg](https://ffmpeg.org/). You can either use the FFmpeg addon from tinyMediaManager (available on Windows, macOS and Linux x64) or an installed version of FFmpeg on your system.

To use this feature, you have to choose which FFmpeg version to use (Settings -> System -> FFmpeg):

+ Download an use the FFmpeg addon from the tinyMediaManager repository 

or

+ The location of the FFmpeg binary from your system (which will be called from tinyMediaManager)

After you have chosen the FFmpeg binary, you can create thumbs/stills with a new "artwork scraper" which creates a bunch of stills which aims to provide multiple different captures of the video file. To activate the FFmpeg scraper, you need to go to:

**Movies**: Settings -> Movies -> Artwork  
**TV shows**: Settings -> TV shows -> Artwork  

and activate the FFmpeg scraper. You can also specify how these stills will be created. 

Enabling all needed settings, you can either let tinyMediaManager automatically create and choose (just random - no algorithm for detecting a "good" still has been included) a still by using the automatic artwork download **or** go into the respective editor, click on the artwork type you want to get stills and choose the one you like.

**Note**: This does not work with ISO files or DVD/BluRay structures
{: .notice--warning}