---
title: Regular Expression Based JMTE Annotation
permalink: /blog/jmte-regular-expression/
categories:
  - News
tags:
  - v4
  - v4.3
summary:
  - image: tmm.png
---

tinyMediaManager v4.3 added a way to enhance the [JMTE](/docs/jmte/) syntax with regular expressions. You can use this regular expression annotation to extract/modify parts of the chosen token. 

A common use case is to preserve some _tags_ of a file name which tinyMediaManager cannot handle. Just imagine you have a file

```
movie-title [tag1] [tag2].mkv
```

and you want to preserve the `[tag1] [tag2]`part of the original file name in the renamer.

## Annotation syntax

The syntax of the annotation is as follows:

`${@regexp expression source destination}`

where 

* `${@regexp` - opens the annotation for JMTE
* `expression` - is the regular expression. **NOTE**: you cannot use `{}` in the regular expression since those will conflict with JMTE
* `source` - the source token where to take the information from. **ATTENTION**: you need to insert the whole token (and not the short token). E.g. `movie.originalFilename`
* `destination` - a string with placeholders where the found parts of the regular expression will be entered. `$1` will use the findings from group 1, `$2`will use findings from group 2, ...

JMTE uses the Java syntax for regular expression. You can test your regular expression with the online tool [RegexPlanet](https://www.regexplanet.com/advanced/java/index.html).
{: .notice--info}

## Example

Having the file name `movie-title [tag1] [tag2].mkv` and the annotated pattern `${@regexp (\[.*?\])\. movie.originalFilename $1}` will produce `[tag1] [tag2]`. 

This can be enhanced by other JMTE tokens:

```
${title} ${- ,edition,} (${year}) ${@regexp (\[.*?\])\. movie.originalFilename $1}
```

will produce (with the title **Foo** and year **2008**): `Foo (2008) [tag1] [tag2]`.


