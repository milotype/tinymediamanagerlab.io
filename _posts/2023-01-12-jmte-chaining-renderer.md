---
title: JMTE Chaining Renderer
permalink: /blog/jmte-chaining-renderer/
categories:
  - News
tags:
  - v4
  - v4.3
summary:
  - image: tmm.png
---

Starting with v4.3.8 tinyMediaManager offers a new JMTE renderer which allows the usage multiple JMTE renderers for one token. The syntax for the new renderer is:

```
${token;chain(renderer1(parameters1);renderer2(parameters2);...)}
```

The new renderer is called `chain` and takes other renderers with its options as parameters. All chained renderers need to be separated via `;`.

E.g. if you want to render a movie to remove German umlauts from the title and morph the result to UPPER CASE, you can use the following syntax:

```
${movie.title;chain(replace(umlauts.csv);upper)}
```