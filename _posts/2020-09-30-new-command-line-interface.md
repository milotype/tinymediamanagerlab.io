---
title: new command line interface (v4)
permalink: /blog/new-command-line-interface/
categories:
  - News
tags:
  - v4
summary:
  - image: tmm.png
---

The command line interface (CLI) has been rewritten for v4 using the [picocli](https://picocli.info/) library. The new CLI has some advantages over the old CLI (v3):

- trigger tmm update from command line is possible now
- better handling of commands and subcommands/parameters
- better integration in tinyMediaManager (cleaner code, better maintainability)
- nicer help

But there is also a little drawback: you can't combine command from different modules any more (call movie and TV show commands together).

A call to the new command line interface looks like now:

`tinyMediaManager movie -u -n -r` to find/scrape and rename new movies

`tinyMediaManager movie -t -s` to download missing trailer/subtitles

`tinyMediaManager movie -e -eT=ExcelXml -eP=/user/export/movies` to export the movie list with the ExcelXml template to /user/export/movies


`tinyMediaManager tvshow -u -n -r` to find/scrape and rename new TV shows/episodes

`tinyMediaManager tvshow -t -s` to download missing trailer/subtitles

`tinyMediaManager tvshow -e -eT=TvShowDetailExampleXml -eP=/user/export/tv` to export the TV show list with the TvShowDetailExampleXml template to /user/export/tv

See the full documentation about the new command line interface at our [docs](/docs/commandline/).
