---
title: Version v3.1.12
permalink: /blog/version-3-1-12/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
---
\+ added "_" as a colon separator in the renamer settings #974  
x backport MediaInfo changes from v4 (aspect ratio and others)  
x backport fixes for trailer downloading from v4  
x detecting video resolution for certain 3D SBS files #1133  
