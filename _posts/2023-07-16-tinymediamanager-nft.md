---
title: tinyMediaManager NFTs
permalink: /blog/tinymediamanager-nft/
categories:
  - News
tags: 
  - link
link: https://opensea.io/collection/tinymediamanager
---

Originally, generative art was just a fun project in my spare time - however, I soon realized that you could also create unique images based on characteristics in the tinyMediaManager code.

So I followed this approach and created a unique fingerprint for each release of tinyMediaManager, which is the data basis for a new artwork. Combining this fingerprint with different generative algorithms, it is possible to create a unique artwork for each release of tinyMediaManager.

I decided to share these artworks with the community by offering them as NFTs at [OpenSea](https://opensea.io/collection/tinymediamanager).

By owning such an NFT you not only hold a unique part of tinyMediaManager (each artwork can be created exactly once by the algorithm and is therefore unique), you also secure a lifetime access to all PRO features of tinyMediaManager!

Up to now 32 releases have been put online, the others will be put online bit by bit (after new algorithms have been implemented)! Artworks for the releases Alpha 1 - v3 will be put online as an auction with a duration of 3 months - the artworks from v4 on will be put online as an auction for one month each.

**To all tinyMediaManager fans out there: get a piece of tinyMediaManager history: [https://opensea.io/collection/tinymediamanager](https://opensea.io/collection/tinymediamanager)**