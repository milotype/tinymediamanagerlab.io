---
title: Version v3.1.9
permalink: /blog/version-3-1-9/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
---
x removed kyradb.com artwork provider  
x take the correct artwork extension when using local files in the image chooser  
x several enhancements to the AniDB scraper
