---
title: Version 2.9.2
permalink: /blog/version-2-9-2/
categories:
  - News
  - Release
tags:
  - v2    
summary:
  - image: release.png
---
\+ added action: download missing artwork  
\+ added parsing of Synology VSMETA files  
\+ added shortcut (CTRL - F) to jump to the search field (thx @hampelratte)  
\+ improved user interaction for tags and genres (thx @hampelratte)  
\+ get a list of video files for TV show export with episode.videoFiles  
\+ added HDRip as an own media source  
\+ movie renamer: added $K token (first tag)  
\+ added moving of genres/tag in the movie editor  
x fixed localized artwork loading for brazilian users  
x fixed deleting of backup folder in windows #120  
x fixed detection/renaming of movieset artwork in movie folders #111  
x do not remove movie set artwork when removing movies from tmm #111  
x do not remove movie sets when removing movies/data sources from tmm #111  
x increased max memory setting for 64bit users to 2560MB  
x updated MediaInfo to 0.7.91  
x fixed parsing of media source  
