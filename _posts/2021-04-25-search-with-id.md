---
title: Search with TMDB/IMDB/TVDB id
permalink: /blog/search-with-id/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---

Starting with tinyMediaManager v4.1.4 it is now possible to do a movie/TV show search using the TMDB/IMDB/TVDB id directly in the search window. To make this work, you have to prefix the id with the name of the metadata provider:

* **TMDB** -> TMDB:xxxxxx
* **IMDB** -> IMDB:ttxxxxxx (or ttxxxxxx) directly
* **TVDB** -> TVDB:xxxxxx (TV shows only)

<a class="fancybox" href="{{ site.urlimg }}2021/04/search_with_id.png" rel="post" title="Searching with TMDB id">![Searching with TMDB id]({{ site.urlimg }}2021/04/search_with_id.png "Searching with TMDB id"){: .align-center}</a>

At the moment only a subset of scrapers are able to search with those ids:

* TMDB -> themoviedb.org
* IMDB -> imdb.com, themoviedb.org, omdbapi.com, moviemeter.nl, ofdb.de    
* TVDB -> thetvdb.com, themoviedb.org