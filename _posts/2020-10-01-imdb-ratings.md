---
title: IMDB ratings
permalink: /blog/imdb-ratings/
categories:
  - News
tags:
  - v4
summary:
  - image: tmm.png
---

Using the official IMDB datasets it is possible to mix in the IMDB ratings into every scrape where an IMDB id is available. The IMDB id is provided by the following scrapers:

- The Movie Database (TMDb)
- Trakt.tv
- MPDb.TV
- OFDb.de
- Omdbapi.com
- Moviemeter.nl
- The TVDB

If you just need ratings from IMDB, you no more need to use the universal scrapers.
