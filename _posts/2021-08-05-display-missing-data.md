---
title: Display missing metadata
permalink: /blog/display-missing-metadata/
categories:
  - News
tags:
  - v4
  - v4.2
summary:
  - image: tmm.png
---

tinyMediaManager v4.2 introduces a new way to show you what kind of metadata is missing. In the past this check was kinda hard coded inside tinyMediaManager and the corresponding column (called NFO) was more or less just an indicator if there is a NFO file available or not - which is a rather useless information about the quality of the data for your library.

To enhance this we completely reworked this section by doing the following things:

* remove the old NFO column logic
* add a column to indicate which metadata is missing
* add a setting where the user can define his own definition of "missing/complete" metadata
* add a tooltip for this new column indicating which fields are missing (regardless of the setting above)

While refactoring this for metadata we also changed the same logic for artwork (which was more or less already there, but not as good as the new solution).

## Configuration

With the new implementation you will get two different information: 

* which metadata/artwork is missing according to your settings
* which metadata/artwork is missing in general

For the first information there is a new area in the tinyMediaManager settings directly under:

* Movies
<a class="fancybox" href="{{ site.urlimg }}2021/08/movie_settings.png" rel="post" title="Movie settings">![Movie settings]({{ site.urlimg }}2021/08/movie_settings.png "Movie settings"){: .align-center}</a>

* Movie sets
<a class="fancybox" href="{{ site.urlimg }}2021/08/movieset_settings.png" rel="post" title="Movie set settings">![Movie set settings]({{ site.urlimg }}2021/08/movieset_settings.png "Movie set settings"){: .align-center}</a>

* TV shows
<a class="fancybox" href="{{ site.urlimg }}2021/08/tvshow_settings.png" rel="post" title="TV show settings">![TV show settings]({{ site.urlimg }}2021/08/tvshow_settings.png "TV show settings"){: .align-center}</a>

These values will have an influence how the checkmark in the in the metadata/artwork column will be calculated.

## Tooltips

In addition to the checkmark (which indicates what metadata/artwork is missing for your needs), tinyMediaManager also offers a tooltip which metadata in general is missing (this is not configurable - it is just useful to indicate what tinyMediaManager _could_ manage but is missing):

<a class="fancybox" href="{{ site.urlimg }}2021/08/movie_missing_metadata.png" rel="post" title="Movie: missing metadata">![Movie: missing metadata]({{ site.urlimg }}2021/08/movie_missing_metadata.png "Movie: missing metadata"){: .align-center}</a>

<a class="fancybox" href="{{ site.urlimg }}2021/08/movie_missing_artwork.png" rel="post" title="Movie: missing artwork">![Movie: missing artwork]({{ site.urlimg }}2021/08/movie_missing_artwork.png "Movie: missing artwork"){: .align-center}</a>

<a class="fancybox" href="{{ site.urlimg }}2021/08/tvshow_missing_metadata.png" rel="post" title="TV show: missing metadata">![TV show: missing metadata]({{ site.urlimg }}2021/08/tvshow_missing_metadata.png "TV show: missing metadata"){: .align-center}</a>

<a class="fancybox" href="{{ site.urlimg }}2021/08/tvshow_missing_artwork.png" rel="post" title="TV show: missing artwork">![TV show: missing artwork]({{ site.urlimg }}2021/08/tvshow_missing_artwork.png "TV show: missing artwork"){: .align-center}</a>




