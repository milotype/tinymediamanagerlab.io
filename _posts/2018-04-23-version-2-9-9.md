---
title: Version 2.9.9
permalink: /blog/version-2-9-9/
categories:
  - News
  - Release
tags:
  - v2  
summary:
  - image: release.png
---
\+ removed usage of Google Analytics  
\+ added hebrew as media language (thx to @israelio)  
x several fixes in the OSX starter script  
x fixed opening of dialogs without parents  
x fixed occasional errors on closing  
x fixed downloading extrafanart/extrathumbs after network failures  
x improved downloading of artwork from movie/TV show editors  
x fixed auto select the first search result in the search windows  
x fixed sorting of movies in movie sets  
x fixed IMDB parsing from movie NFOs  
x improved IMDB scraping original titles  
x improved episode title detection (cleanup)   
