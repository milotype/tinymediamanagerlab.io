---
title: version v3.0 RC3
permalink: /blog/version-3-0-RC3/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
---

tinyMediaManager v3 - RC3 is out containing many fixes/improvements over RC2:

\+ major rework of filters  
\+ added subtitle column for TV shows  
\+ MI: gather title & real file creation date  
\+ reworked image cache task to use all CPUs  
\+ reworked caching Url classes for better caching  
\+ bulk editor -> possibility to remove all entry from the lists (thx to @wjanes)  
x episodes: write correct XML header  
x showtitle in episode NFO now contains the show title  
x logging enhancements  
x deletion of old backup files  
x redesigned valid NFO evaluation  
x do not return null on search (prevent NPE)
x KodiRPC: better matching  
x update filters on data change in the TV show tree  
x changed window management back to the system style  
x re-colored the new indicator  
x align numbers (rating, size, ...) right sided - movie/TV show tab  
x prevent some flickering when scraping  
x fixed NPE in ImageLabel  
x force overwrite actor images on re-scrape  
x import V2 datasources from backup  
x corrected url to the donation page  
x do not delete ratings with 0 votes in the editors  
x display set certificate in editors  
x better resetting of the image cache for actor artwork  
x init dummy episodes even if setting is off  
x try to use the date settings from the system for UI display  
x re-create renamer examples when activating season specials  
x suppress 0 to be rendered into file names  
x use a special comparator for original title sorting  
x feature: added British English UI language (primary for date formatting)  
x changed header icon for certification  
x MediaInfo: always check response ;) yes/no vs true/false  
x Update to latest official Getdown snapshot  
x Refactor audio streams channels String->int; introduce "default"  
x MainVideoFile = BiggestVideoFile  
x MediaInfo: fix channel decoding for various MI versions  
x prevent episodes to be added twice to a season  
x respect ascii replacement with the _first_ renderer  
x remove empty seasons on the fly  
x fix dateAdded  
x changed setting watched state of tvshows and settings to toggle (thx to @wjanes)  
x MediaInfo: fix DTS-ES and channel detection  


Please report any issues/feature requests at [our project home](https://gitlab.com/tinyMediaManager/tinyMediaManager/).
