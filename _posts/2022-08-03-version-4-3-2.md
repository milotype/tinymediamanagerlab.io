---
title: Version v4.3.2
permalink: /blog/version-4-3-2/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
\+ redesign artwork type naming panel  
x check for existing ID when using MPDb.TV scraper (metadata & artwork)  
x fixed aspect ratio detection; improved logging of errors  
x fixed occasional removing of cast when re-scrape #1840  
x fixed downloading of some YT trailers  
x fixed fetching of season artwork from TVDB #1841  
x fixed KodiRPC calls #1831  
x download missing artwork did not respect the chosen scrapers  
x improved fallback episode detection for files with just numbers  
x improved error handling on reading MediaInfo XMLs  
x revert UI lib to fix scaling issues with bigger fonts  
x hd-trailers.net better error handling  
x fix FFMpeg artwork scraper for tv episodes #1842  
x parse even more metadata fields of MKV files  