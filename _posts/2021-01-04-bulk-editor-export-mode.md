---
title: Expert mode in bulk editor
permalink: /blog/bulk-editor-expert-mode/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---

tinyMediaManager v4.1 introduces a new expert mode in the bulk editor to _freely_ set some pre-defined text fields in the movie. This expert mode allows you to set the value of a text based field in the movie with [JMTE](/docs/jmte/) syntax. Using [JMTE](/docs/jmte/) here allows you to access all other information of the movie including the associated files to fill the target field in the movie.

To use the expert mode in the bulk editor, just select all movies you want to edit, then do a right click an choose "Bulk editing". In the opened dialog go to the tab "Expert". 

<a class="fancybox" href="{{ site.urlimg }}2021/01/bulk_editor.png" rel="post" title="Bulk editor - Expert mode">![Expert mode]({{ site.urlimg }}2021/01/bulk_editor.png "Bulk editor - Expert mode"){: .align-center}</a>

In this expert mode, you have the option to fill the text based fields:

* Title
* Original title
* Sorttitle
* Tagline
* Plot
* Production company
* Spoken languages
* Country
* Note
* Original filename

with values from a [JMTE](/docs/jmte/). You can use the same values as in the [movie renamer](/docs/movies/settings/#renamer-pattern) or the [export templates](/docs/exporttemplates/).

Below the input field for the pattern you see all old values of the selected movies and their new values which would be generated with the [JMTE](/docs/jmte/) pattern. To apply the values, simply click the apply button right to the input field.