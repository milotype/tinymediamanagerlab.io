---
title: TheMovieDb - Title/Overview fallback language.
permalink: /blog/tmdb-fallback-language/
categories:
  - News
  - Feature
summary:
  - image: tmdb.png
---

In version 2.9.4 we introduced a new feature that applies to the "TheMovieDb" scraper - you are now able to have more flexible results on your movie and TV shows scraping.

This feature allows you to define a secondary scraping language, in case there are no localized results in your primary scraping language.

## How does it work?

When you make a query to TheMovieDb service it returns the results either in the specified scraping language (taken from the tinyMediaManager settings) or in the movie's original language translation. E.g. if you want to scrape a French movie in Greek, but there is no Greek translation for that movie, TheMovieDb will return you the French results for this movie.

As you can imagine not all Greek users understand French - but most of them would understand English as a fallback if there is no Greek content available.

Now with this new feature we made some differences in the scrape functionality. With this feature activated, tinyMediaManager checks every result if the returned result is in the desired language or not. If the result is in your preferred language, the scraper returns the original result, if it is in the movies original language, the scraper will make an additional query on your fallback language.

Most of the content on TheMovieDb is localized in English. With English as the fallback language you would certainly get most of the time localized content in English if there is no content in your primary language. If the fallback language is anything other than English (then even with this feature activated) and the content is not localized on your preferred language neither on fallback language, you will end up with the same results with an extra query, which makes the scraping slow.

For that reason fallback system by default is **disabled**.

## Set Up

The configuration of this feature can be found in the settings (movies and TV shows) -> scraper -> themoviedb.org.
There are now two more configurable fields:

* **titleFallback** enable/disable this feature.
* **titleFallbackLanguage** select the desired fallback language.

<a class="fancybox" href="{{ site.urlimg }}2017/08/tmdb-fallback1.png" rel="post" title="Settings Panel">![Settings Panel 1]({{ site.urlimg }}2017/08/tmdb-fallback1.png "Settings Panel")</a>
{: refcent}

## Results

This is an example with fallback disabled and enabled on the following screenshots

<a class="fancybox" href="{{ site.urlimg }}2017/08/tmdb-fallback-disabled.png" rel="post" title="Fallback Disabled Results">![Fallback Disabled Results]({{ site.urlimg }}2017/08/tmdb-fallback-disabled.png "Fallback Disabled Results")</a>
{: refcent}

<a class="fancybox" href="{{ site.urlimg }}2017/08/tmdb-fallback-enabled.png" rel="post" title="Fallback Enabled Results">![Fallback Enabled Results]({{ site.urlimg }}2017/08/tmdb-fallback-enabled.png "Fallback Enabled Results")</a>
{: refcent}


## Drawbacks

Enabling this feature makes additional queries to TheMovieDb service if there is no localized content available and therefore you may hit the connection limit of this service more often (resulting in a forced waiting time).

Also as mentioned earlier, if you select as fallback language a language beside English, and the content you are trying to fetch is not localized on English, you will end up again with the same results as your preferred language, with the exception that you will make an additional query.

---
You can navigate to [GitHub's Pull Request Page](https://github.com/tinyMediaManager/scraper-tmdb/pull/4) for more technical details.

This feature is made by [Nikolas Mavropoylos](https://github.com/ProIcons) for the tinyMediaManager project.
