---
title: Version v4.3.11
permalink: /blog/version-4-3-11/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
\+ added Chinese (Singapore) to the scraper languages #2142  
\+ (renamer) audioChannels in "dot" notation (6ch -> 5.1)  
\+ (bulk edit) remember last used tab when bulk editing  
\+ upgraded libmediainfo to 23.04  
\+ added Macedonian to the scraper languages #2170  
\+ (mediainfo) extract subtitles from audio files too #2180  
\+ (Trakt) added an option to choose which date field is used for "collected at" #2168  
x (TMDB) enhanced loading of localized episode content (title, original title, plot) #2165  
x fixed Kodi RPC mappings  
x fixed trailer downloading  
x (Docker) force writing of window sizes/locations on shutdown #2091  
x (TMDB/TVDB) do not scrape empty episode names if no translation is available #2147  
x (movie set) downloading of artwork with different extensions #2154  
x (TV shows) fixed renaming of video extras #2145  
x fix extras detection for TV shows/episodes  
x (movie) fix renaming of stacked subtitles  
x fix renamer preview for extras  
x added a faster way to re-evaluate MMD on each update  
x made update data sources more crash resistant #2169  
x enhanced logic to detect scraped status #2156  
x enhanced extra(s) handling for episodes  
x enhanced multi movie detection  
x fixed pre-set of episode thumbs in the "Scrape metadata of selected episodes" dialog  
x moved version info into the toolbar #2161  
x added "Canceled" status for TV shows #2177  
x improved edition detection from filename  
x some enhancements on the automatic artwork download algorithm  