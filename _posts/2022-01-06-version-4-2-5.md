---
title: Version v4.2.5
permalink: /blog/version-4-2-5/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
\+ enabled aspect ratio detection for disc folders (VIDEO_TS, BDMV) #1573  
\+ added an option to ignore specials in list missing episodes (thx @dimitricappelle)  
x upgraded Java to 15.0.5  
x re-ordered popup menus #1400  
x add "path" column to TV show table settings  
x download missing artwork when saving movies/TV shows/seasons/episodes #1583  
x fixed an issue with the FFmpeg artwork provider #1587  
x consider external files when getting MediaInfo for movie/show #1601  
x fix downloading trailer, if show has not been cleaned yet #1600  
x fix checking for missing season names #1602  
x (linux) recompiled the launcher to be run with glib < 2.32 #1597  
x remove movies/episodes in background (do not block the UI) #1596  
x (TVDB/TMDB) use only configured languages for title/plot #1605  
x reworked image downloading engine for less needed downloads and better artwork filename cleanup #1598  
x fixed loading of season fanart on startup #1607  
x run aspect ratio detection for episodes too #1608  
x added automatic text direction detection #1595  
x further improve naming detection #1614  
x (TVDB/TMDB) add networks in the front of the production companies #1569  