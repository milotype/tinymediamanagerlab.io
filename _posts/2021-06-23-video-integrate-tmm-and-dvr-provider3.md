---
title: Video - How to integrate tinyMediaManager in DVR-Provider 3 
permalink: /blog/video-how-to-integrate-tmm-in-dvr-provider3/
categories:
  - in media
tags: 
  - link
link: https://www.youtube.com/watch?v=ZSlmc1U-hpk
---

Haenlein Software has made a video on [YouTube](https://youtube.com) which shows how to integrate the results of tinyMediaManager in DVR-Provider 3:
