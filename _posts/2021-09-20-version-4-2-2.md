---
title: Version v4.2.2
permalink: /blog/version-4-2-2/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
\+ added selected entries amount and file size totals to the movie/movie set/TV show list #1450  
\+ added file size totals tp the movie/movie set/TV show table  
\+ added an option to set the default value for "do not overwrite existing data" to the settings  
\+ added a setting to ignore completeness check for specials  
x (TVDB) fix recent API changes from TVDB #1481  
x (TVDB) enhanced translation handling of our API implementation  
x fix some minor issues for the live update of movie set data #1477  
x calculation of minimum size for restoring window sizes/positions #1485  
x enhanced loading times of internal scrapers  
x fix detection of paths in skip folders #1493  
x fix detection of multiple trailers (Zidoo style) #1398 #1413  
x lowered defaults for completeness check  
x enhanced the filename filter to run against all media files  
x rewrote fetching of translated episodes in the episode chooser  
x (IMDB) allow unassigned episodes to be scraped #1494  
x several fixes for post-processing management  