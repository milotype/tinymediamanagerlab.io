---
title: Install tinyMediaManager via docker on a Synology NAS
permalink: /blog/tmm-docker-synology/
categories:
  - in media
tags: 
  - link
link: https://mariushosting.com/how-to-install-tinymediamanager-on-your-synology-nas/  
---

The site [mariushosting.com](https://mariushosting.com) has made a tutorial how to install tinyMediaManager via docker on a Synology NAS:
