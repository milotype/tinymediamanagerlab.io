---
title: New episodeguide Format
permalink: /blog/new-episodeguide-format/
categories:
  - News
tags:
  - v4
  - v4.3
summary:
  - image: tmm.png
---

Kodi (and its scrapers) has some drawbacks for the \<episodeguide\> information which will be written in the NFO files. The information in the \<episodeguide\> field helped Kodi to find online information for episodes **when no** episode NFO files have been written (which is not the default case for tinyMediaManager).

Recently there were some changes within Kodi scrapers which enabled a new way to provide the \<episodeguide\> information - see [https://forum.kodi.tv/showthread.php?tid=370489](https://forum.kodi.tv/showthread.php?tid=370489) for more information.

### Activate the new format
tinyMediamanager v4.3.8+ now provides a setting to write the new style which can be read by the Kodi scrapers. The new setting is located at "Settings -> TV show settings -> NFO settings":

<a class="fancybox" href="{{ site.urlimg }}2022/12/episodeguide.png" rel="post" title="New episodeguide format">![New episodeguide format]({{ site.urlimg }}2022/12/episodeguide.png "New episodeguide format"){: .align-center}</a>


### Create new NFO files
You just need to activate this option, select all TV shows and "right click -> Enhanced editing -> Rewrite NFO files for selected TV show(s)" to rewrite a new NFO file containing the new \<episodeguide\> format (make sure your storage is connected when rewriting NFO files!).