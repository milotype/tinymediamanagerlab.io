---
title: Version v4.3.11.1
permalink: /blog/version-4-3-11-1/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
x some fixes/enhancements on the automatic artwork download algorithm #2182, #2184  
x use "date added" as default date for Trakt.tv sync #2183  
x (Trakt) added sanity check: we must not sync a date in the future #2183  